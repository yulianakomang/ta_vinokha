/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `tb_busanatari`;
CREATE TABLE `tb_busanatari` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_kategori` varchar(15) DEFAULT '',
  `kode` char(8) DEFAULT '',
  `nama` varchar(50) DEFAULT '',
  `size` varchar(15) DEFAULT '',
  `keterangan` varchar(200) DEFAULT '',
  `harga` int(15) DEFAULT NULL,
  `foto` varchar(50) DEFAULT '',
  `status` int(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_detailsewa`;
CREATE TABLE `tb_detailsewa` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_sewa` int(11) DEFAULT NULL,
  `id_busana` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tb_fotobusana`;
CREATE TABLE `tb_fotobusana` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_busana` int(10) DEFAULT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tb_kategori`;
CREATE TABLE `tb_kategori` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_pengembalian`;
CREATE TABLE `tb_pengembalian` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_sewa` int(10) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_sewa`;
CREATE TABLE `tb_sewa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` int(12) DEFAULT NULL,
  `kode` char(12) DEFAULT '',
  `tgl_pinjam` date DEFAULT NULL,
  `tgl_pengembalian` date DEFAULT NULL,
  `sub_total` int(15) DEFAULT NULL,
  `status_penyewaan` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `online` int(11) DEFAULT '0',
  `bukti_tf` varchar(200) DEFAULT NULL,
  `setujui` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_transaksi`;
CREATE TABLE `tb_transaksi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_sewa` int(10) DEFAULT NULL,
  `nominal` int(15) DEFAULT NULL,
  `keterangan` text,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `level` varchar(25) DEFAULT '3',
  `nama` varchar(100) DEFAULT '',
  `no_telp` char(20) DEFAULT '',
  `alamat` varchar(100) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `no_ktp` char(30) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `user_image` varchar(255) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO `tb_busanatari` (`id`,`nama`,`foto`,`id_kategori`,`created_at`,`size`,`stok`,`updated_at`,`keterangan`,`harga`,`status`,`kode`) VALUES 
 ('4', 'Tari Cendrawasih', '1579081263.jpg', '3', '2020-01-15 09:41:03', 'Besar', '13', '2020-01-16 08:18:08', 'warna merah', '75000', '1', 'BT-94818'),
('5', 'Tari Sekar Sanjiwani', '1579097476.jpg', '3', '2020-01-15 09:44:16', 'Besar', '15', '2020-01-18 15:05:04', NULL, '75000', '1', 'BT-22636'),
('6', 'Tari Gadung Kasturi', '1579097494.jpg', '3', '2020-01-15 09:48:36', 'Besar', '15', '2020-01-15 14:11:34', NULL, '125000', '1', 'BT-96181'),
('7', 'Tari Legong Kraton', '1579097513.jpg', '3', '2020-01-15 09:58:02', 'Besar', '15', '2020-01-15 14:11:53', NULL, '125000', '1', 'BT-43238'),
('8', 'Tari Briuk Tinjal', '1579096932.jpg', '1', '2020-01-15 14:02:12', 'Kecil', '10', '2020-01-15 14:02:12', NULL, '50000', '1', 'BT-36936'),
('9', 'Tari Kasmaran', '1579097648.jpg', '3', '2020-01-15 14:14:08', 'Besar', '10', '2020-01-15 14:14:08', NULL, '125000', '1', 'BT-26948'),
('10', 'Tari Dupa Puja', '1579097858.jpg', '3', '2020-01-15 14:17:38', 'Besar', '15', '2020-01-15 14:17:38', NULL, '75000', '1', 'BT-63879'),
('11', 'Tari Shanti', '1579097894.jpg', '4', '2020-01-15 14:18:14', 'Besar', '15', '2020-01-18 14:49:18', NULL, '100000', '1', 'BT-55651'),
('12', 'Tari Ngulati Rahayu', '1579098078.jpg', '4', '2020-01-15 14:21:18', 'Besar', '7', '2020-01-17 03:13:44', NULL, '150000', '1', 'BT-33701'),
('13', 'Tari Legong Gana Lila', '1579098144.jpg', '4', '2020-01-15 14:22:24', 'Besar', '10', '2020-01-15 14:22:24', NULL, '150000', '1', 'BT-83644'),
('14', 'Tari Legong Mesatya', '1579098867.PNG', '4', '2020-01-15 14:34:27', 'Besar', '10', '2020-01-15 14:34:27', NULL, '150000', '1', 'BT-41006'),
('15', 'Tari Gandrung Kecil', '1579098986.jpg', '1', '2020-01-15 14:35:52', 'Kecil', '10', '2020-01-15 14:36:26', NULL, '100000', '1', 'BT-32252'),
('16', 'Tari Gandrung Besar', '1579099159.PNG', '1', '2020-01-15 14:39:19', 'Besar', '10', '2020-01-15 14:39:19', NULL, '100000', '1', 'BT-77384'),
('17', 'Tari Siyer Mmale', '1579099249.jpg', '1', '2020-01-15 14:40:49', 'Besar', '10', '2020-01-15 14:40:49', NULL, '100000', '1', 'BT-93335'),
('18', 'Tari Jauk Manis', '1579099302.jpg', '3', '2020-01-15 14:41:42', 'Besar', '10', '2020-01-15 14:41:42', NULL, '150000', '1', 'BT-13385'),
('19', 'Tari Sekar Jagat', '1579099389.jpg', '3', '2020-01-15 14:43:09', 'Besar', '12', '2020-01-16 14:15:20', NULL, '125000', '1', 'BT-85268'),
('20', 'Tari Cendrawasih + Make Up', '1579099690.jpg', '3', '2020-01-15 14:48:10', 'Besar', '15', '2020-01-15 14:48:10', NULL, '150000', '1', 'BT-42851'),
('21', 'Tari Sekar Sanjiwani + Make Up', '1579099824.jpg', '3', '2020-01-15 14:49:17', 'Besar', '15', '2020-01-15 14:50:24', NULL, '125000', '1', 'BT-54879'),
('22', 'Tari Gadung Kasturi + Make Up', '1579099891.jpg', '3', '2020-01-15 14:51:31', 'Besar', '15', '2020-01-25 13:57:35', NULL, '175000', '1', 'BT-92327'),
('23', 'Tari Sekar Jagat', '1579183795.jpg', '3', '2020-01-16 14:09:55', 'Besar', '15', '2020-01-16 14:09:55', NULL, '125000', '1', 'BT-89027'),
('24', 'Tari Pendet', '1579183841.jpg', '1', '2020-01-16 14:10:41', 'Besar', '20', '2020-01-16 14:10:41', NULL, '75000', '1', 'BT-64615'),
('25', 'Tari Sekar Jempiring', '1579183889.jpg', '3', '2020-01-16 14:11:29', 'Besar', '15', '2020-01-16 14:11:29', NULL, '125000', '1', 'BT-33574');

INSERT INTO `tb_detailsewa` (`created_at`,`id`,`id_sewa`,`updated_at`,`jumlah`,`id_busana`) VALUES 
 ('2020-01-13 08:53:52', '3', '8', '2020-01-13 08:53:52', '2', '2'),
('2020-01-13 13:17:18', '4', '9', '2020-01-13 13:17:18', '1', '2'),
('2020-01-15 13:10:29', '9', '16', '2020-01-15 13:10:29', '2', '4'),
('2020-01-16 14:15:05', '11', '17', '2020-01-16 14:15:05', '3', '19'),
('2020-01-16 14:20:39', '12', '18', '2020-01-16 14:20:39', '2', '20'),
('2020-01-16 16:11:08', '13', '21', '2020-01-16 16:11:08', '3', '22'),
('2020-01-17 03:13:33', '14', '24', '2020-01-17 03:13:33', '3', '12'),
('2020-01-17 03:20:03', '15', '26', '2020-01-17 03:20:03', '1', '21'),
('2020-01-17 04:20:51', '16', '31', '2020-01-17 04:20:51', '1', '5'),
('2020-01-17 04:21:02', '17', '32', '2020-01-17 04:21:02', '1', '5'),
('2020-01-17 04:22:16', '18', '33', '2020-01-17 04:22:16', '1', '5'),
('2020-01-17 04:23:05', '19', '34', '2020-01-17 04:23:05', '1', '5'),
('2020-01-17 04:29:37', '20', '35', '2020-01-17 04:29:37', '3', '5'),
('2020-01-17 04:29:37', '21', '35', '2020-01-17 04:29:37', '1', '4'),
('2020-01-18 14:02:57', '22', '36', '2020-01-18 14:02:57', '2', '6'),
('2020-01-18 14:03:15', '23', '37', '2020-01-18 14:03:15', '2', '6'),
('2020-01-18 14:04:56', '24', '38', '2020-01-18 14:04:56', '1', '8'),
('2020-01-18 14:05:41', '25', '39', '2020-01-18 14:05:41', '1', '8'),
('2020-01-18 14:08:01', '26', '40', '2020-01-18 14:08:01', '1', '7'),
('2020-01-18 14:10:42', '27', '41', '2020-01-18 14:10:42', '2', '5'),
('2020-01-18 14:19:11', '28', '42', '2020-01-18 14:19:11', '1', '11'),
('2020-01-18 14:53:49', '29', '43', '2020-01-18 14:53:49', '2', '5'),
('2020-01-22 15:08:50', '30', '44', '2020-01-22 15:08:50', '1', '4');

INSERT INTO `tb_fotobusana` (`created_at`,`id`,`foto`,`updated_at`,`id_busana`) VALUES 
 (NULL, '12', 'album-52172-1579448130.png', NULL, '4'),
(NULL, '13', 'album-92744-1579448130.jpeg', NULL, '4'),
(NULL, '14', 'album-94162-1579448130.jpg', NULL, '4'),
(NULL, '15', 'album-92349-1579448130.jpeg', NULL, '4'),
(NULL, '16', 'album-40017-1579448130.png', NULL, '4'),
(NULL, '17', 'album-40218-1579448130.jpeg', NULL, '4'),
(NULL, '18', 'album-11379-1579448130.jpeg', NULL, '4');

INSERT INTO `tb_kategori` (`created_at`,`id`,`updated_at`,`nama`) VALUES 
 (NULL, '1', '2020-01-15 09:39:54', 'Tari Sasak'),
('2019-12-13 03:27:44', '3', '2020-01-13 11:01:39', 'Tari Bali'),
('2020-01-15 09:40:02', '4', '2020-01-15 09:40:02', 'Tari Kreasi');

INSERT INTO `tb_sewa` (`id`,`bukti_tf`,`status_penyewaan`,`created_at`,`setujui`,`tgl_pengembalian`,`tgl_pinjam`,`updated_at`,`id_user`,`online`,`sub_total`,`kode`) VALUES 
 ('8', NULL, '2', '2020-01-13 06:35:58', '1', '2020-01-16', '2020-01-13', '2020-01-15 07:08:30', '1', NULL, '50000', 'SW-67412'),
('11', '1579061593.jpeg', '0', '2020-01-15 04:13:13', '0', '2020-01-19', '2020-01-16', '2020-01-15 04:27:43', '5', '1', '25000', 'SW-79847'),
('12', '1579072239.jpg', '0', '2020-01-15 07:10:39', '0', '2020-01-21', '2020-01-18', '2020-01-15 07:10:39', '5', '1', '50000', 'SW-49997'),
('13', '1579075347.png', '2', '2020-01-15 08:02:27', '1', '2020-01-25', '2020-01-22', '2020-01-15 09:14:05', '5', '1', '50000', 'SW-61576'),
('14', '1579088392.jpg', '2', '2020-01-15 11:39:52', '1', '2020-01-20', '2020-01-17', '2020-01-15 12:38:37', '5', '1', '300000', 'SW-29102'),
('15', '1579088944.jpg', '2', '2020-01-15 11:49:04', '1', '2020-01-18', '2020-01-15', '2020-01-15 11:58:47', '8', '1', '250000', 'SW-48705'),
('16', NULL, '1', '2020-01-15 13:09:10', '1', '2020-01-18', '2020-01-15', '2020-01-15 13:10:53', '9', '0', '150000', 'SW-90499'),
('17', NULL, '1', '2020-01-16 14:14:26', '1', '2020-01-20', '2020-01-17', '2020-01-16 14:15:20', '11', '0', '375000', 'SW-47540'),
('19', NULL, '0', '2020-01-16 14:49:09', '0', '2020-01-19', '2020-01-16', '2020-01-16 14:49:09', '10', '1', '300000', 'SW-99189'),
('21', NULL, '2', '2020-01-16 16:11:08', '1', '2020-01-19', '2020-01-16', '2020-01-25 13:57:35', '11', '0', '525000', 'SW-16961'),
('24', NULL, '1', '2020-01-17 03:13:33', '1', '2020-01-20', '2020-01-17', '2020-01-17 03:13:44', '9', '0', '450000', 'SW-73637'),
('26', NULL, '0', '2020-01-17 03:20:03', '1', '2020-01-20', '2020-01-17', '2020-01-17 03:20:03', '9', '0', '125000', 'SW-88969'),
('27', '1579234372.jpg', '0', '2020-01-17 04:12:52', '0', '2020-01-21', '2020-01-18', '2020-01-17 04:12:52', '5', '1', '75000', 'SW-39936'),
('28', NULL, '0', '2020-01-17 04:13:42', '0', '2020-01-21', '2020-01-18', '2020-01-17 04:13:42', '5', '1', '75000', 'SW-39936'),
('29', NULL, '0', '2020-01-17 04:20:03', '0', '2020-01-21', '2020-01-18', '2020-01-17 04:20:03', '5', '1', '75000', 'SW-39936'),
('30', NULL, '0', '2020-01-17 04:20:21', '0', '2020-01-21', '2020-01-18', '2020-01-17 04:20:21', '5', '1', '75000', 'SW-39936'),
('31', NULL, '0', '2020-01-17 04:20:51', '0', '2020-01-21', '2020-01-18', '2020-01-17 04:20:51', '5', '1', '75000', 'SW-39936'),
('32', NULL, '0', '2020-01-17 04:21:02', '0', '2020-01-21', '2020-01-18', '2020-01-17 04:21:02', '5', '1', '75000', 'SW-39936'),
('33', NULL, '0', '2020-01-17 04:22:16', '0', '2020-02-03', '2020-01-31', '2020-01-17 04:22:16', '5', '1', '75000', 'SW-59758'),
('34', NULL, '0', '2020-01-17 04:23:05', '1', '2020-01-21', '2020-01-18', '2020-01-17 04:26:11', '5', '1', '75000', 'SW-70482'),
('35', '1579235377.jpg', '0', '2020-01-17 04:29:37', '1', '2020-02-01', '2020-01-29', '2020-01-18 13:38:46', '1', '1', '300000', 'SW-51453'),
('36', '1579356177.jpg', '0', '2020-01-18 14:02:57', '0', '2020-01-23', '2020-01-20', '2020-01-18 14:02:57', '5', '1', '250000', 'SW-39150'),
('37', '1579356195.jpg', '0', '2020-01-18 14:03:15', '0', '2020-01-23', '2020-01-20', '2020-01-18 14:03:15', '5', '1', '250000', 'SW-39150'),
('38', '1579356296.jpg', '0', '2020-01-18 14:04:56', '0', '2020-01-24', '2020-01-21', '2020-01-18 14:04:56', '5', '1', '50000', 'SW-13021'),
('39', '1579356341.jpg', '0', '2020-01-18 14:05:41', '1', '2020-01-24', '2020-01-21', '2020-01-18 14:46:31', '5', '1', '50000', 'SW-13021'),
('40', '1579356481.jpg', '0', '2020-01-18 14:08:01', '1', '2020-01-25', '2020-01-22', '2020-01-18 14:43:30', '5', '1', '125000', 'SW-81144'),
('41', '1579356642.jpg', '0', '2020-01-18 14:10:42', '1', '2020-01-24', '2020-01-21', '2020-01-18 14:43:03', '5', '1', '150000', 'SW-41906'),
('42', '1579357151.jpg', '2', '2020-01-18 14:19:11', '1', '2020-01-28', '2020-01-25', '2020-01-18 14:49:18', '5', '1', '100000', 'SW-21650'),
('43', '1579359229.jpg', '2', '2020-01-18 14:53:49', '1', '2020-01-23', '2020-01-20', '2020-01-18 15:05:04', '5', '1', '150000', 'SW-45653'),
('44', '1579705730.jpg', '0', '2020-01-22 15:08:50', '0', '2020-01-26', '2020-01-23', '2020-01-22 15:08:50', '5', '1', '75000', 'SW-20678');

INSERT INTO `tb_transaksi` (`nominal`,`keterangan`,`status`,`id`,`created_at`,`updated_at`,`id_sewa`) VALUES 
 ('1000', 'Uang Muka (DP)', '0', '2', '2020-01-13 08:53:52', '2020-01-13 08:53:52', '8'),
('49000', 'Pelunasan (Pengambilan)', '0', '5', '2020-01-14 14:26:02', '2020-01-14 14:26:02', '8'),
('150000', 'Uang Muka (DP)', '0', '10', '2020-01-15 13:10:29', '2020-01-15 13:10:29', '16'),
('0', 'Pelunasan (Pengambilan)', '0', '11', '2020-01-15 13:10:53', '2020-01-15 13:10:53', '16'),
('375000', 'Uang Muka (DP)', '0', '13', '2020-01-16 14:15:05', '2020-01-16 14:15:05', '17'),
('0', 'Pelunasan (Pengambilan)', '0', '14', '2020-01-16 14:15:20', '2020-01-16 14:15:20', '17'),
(NULL, 'Uang Muka (DP)', '0', '16', '2020-01-16 16:11:08', '2020-01-16 16:11:08', '21'),
('525000', 'Pelunasan (Pengambilan)', '0', '17', '2020-01-16 16:11:32', '2020-01-16 16:11:32', '21'),
(NULL, 'Uang Muka (DP)', '0', '18', '2020-01-17 03:13:33', '2020-01-17 03:13:33', '24'),
('450000', 'Pelunasan (Pengambilan)', '0', '19', '2020-01-17 03:13:44', '2020-01-17 03:13:44', '24'),
(NULL, 'Uang Muka (DP)', '0', '20', '2020-01-17 03:20:03', '2020-01-17 03:20:03', '26'),
('37500', 'Uang Muka (DP)', '0', '21', '2020-01-17 04:21:02', '2020-01-17 04:21:02', '32'),
('37500', 'Uang Muka (DP)', '0', '22', '2020-01-17 04:22:16', '2020-01-17 04:22:16', '33'),
('37500', 'Uang Muka (DP)', '0', '23', '2020-01-17 04:23:05', '2020-01-17 04:23:05', '34'),
('150000', 'Uang Muka (DP)', '0', '24', '2020-01-17 04:29:37', '2020-01-17 04:29:37', '35'),
('125000', 'Uang Muka (DP)', '0', '25', '2020-01-18 14:02:57', '2020-01-18 14:02:57', '36'),
('125000', 'Uang Muka (DP)', '0', '26', '2020-01-18 14:03:15', '2020-01-18 14:03:15', '37'),
('25000', 'Uang Muka (DP)', '0', '27', '2020-01-18 14:04:56', '2020-01-18 14:04:56', '38'),
('25000', 'Uang Muka (DP)', '0', '28', '2020-01-18 14:05:41', '2020-01-18 14:05:41', '39'),
('62500', 'Uang Muka (DP)', '0', '29', '2020-01-18 14:08:01', '2020-01-18 14:08:01', '40'),
('75000', 'Uang Muka (DP)', '0', '30', '2020-01-18 14:10:42', '2020-01-18 14:10:42', '41'),
('50000', 'Uang Muka (DP)', '0', '31', '2020-01-18 14:19:11', '2020-01-18 14:19:11', '42'),
('50000', 'Pelunasan (Pengambilan)', '0', '32', '2020-01-18 14:48:58', '2020-01-18 14:48:58', '42'),
('75000', 'Uang Muka (DP)', '0', '33', '2020-01-18 14:53:49', '2020-01-18 14:53:49', '43'),
('75000', 'Pelunasan (Pengambilan)', '0', '34', '2020-01-18 15:03:08', '2020-01-18 15:03:08', '43'),
('37500', 'Uang Muka (DP)', '0', '35', '2020-01-22 15:08:50', '2020-01-22 15:08:50', '44'),
('1575000', 'Denda! karena lewat6 hari dari seharusnya.', '0', '36', '2020-01-25 13:57:35', '2020-01-25 13:57:35', '21');

INSERT INTO `tb_user` (`alamat`,`no_ktp`,`password`,`id`,`created_at`,`level`,`no_telp`,`nama`,`email`,`user_image`,`updated_at`) VALUES 
 ('Ampenan', '19878291010', '$2y$12$NualV.opu85DIeKZWzAOueqQCale1Q2LLRyHpvRg4w3skO4K1oY1W', '1', '2020-01-16 14:13:13', '3', '088888888888', 'Gung', 'nachahppc@gmail.com', '', '2020-01-16 14:13:13'),
('Jl Majapahit No 12', '12343212310', '$2y$12$NualV.opu85DIeKZWzAOueqQCale1Q2LLRyHpvRg4w3skO4K1oY1W', '4', NULL, '1', '088888888888', 'Asti', 'owner@gmail.com', '', '2020-01-15 11:36:24'),
('Jl Majapahit No 12', '123456789010', '$2y$12$NualV.opu85DIeKZWzAOueqQCale1Q2LLRyHpvRg4w3skO4K1oY1W', '5', NULL, '2', '099999999', 'Renny', 'admin@gmail.com', '', '2020-01-15 11:37:04'),
('Jl. Sistem, No. 500', '93748372423', '$2y$12$NualV.opu85DIeKZWzAOueqQCale1Q2LLRyHpvRg4w3skO4K1oY1W', '8', '2020-01-13 07:35:15', '3', '085737422503', 'Vinokha', 'rahtiaanandapremavinokha@gmail.com', '', '2020-01-15 11:38:53'),
('Narmada', '6543212345610', '$2y$12$NualV.opu85DIeKZWzAOueqQCale1Q2LLRyHpvRg4w3skO4K1oY1W', '9', '2020-01-15 12:42:06', '3', '088888888888', 'Oman Ayu', 'gedeyudaguna@gmail.com', '', '2020-01-15 12:42:06'),
('Cakranegara', '23555555555', '$2y$12$NualV.opu85DIeKZWzAOueqQCale1Q2LLRyHpvRg4w3skO4K1oY1W', '10', '2020-01-15 12:42:58', '3', '081805778448', 'Putu Ayu', 'agrasenayuddhaadriraja@gmail.com', '', '2020-01-15 12:42:58'),
('Ampenan', '19878291010', '$2y$12$NualV.opu85DIeKZWzAOueqQCale1Q2LLRyHpvRg4w3skO4K1oY1W', '11', '2020-01-16 14:13:13', '3', '088888888888', 'Naomi Wijaya', 'rahtiaanandapremavinokha@gmail.com', '', '2020-01-16 14:13:13');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;