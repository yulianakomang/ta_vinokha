Halo {{ $data['sewa']->getUser->nama }}, <br>
Terimakasih sudah booking online penyewaan busana kami. <br>
<br>
Berikut adalah Detail Pemesanan: <br>

<?php 

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}

function status($value='')
{
  $status = array(
    '<button class="btn btn-danger">Belum Diambil</button>',
    '<button class="btn btn-primary">Sudah Diambil</button>',
    '<button class="btn btn-success">Sudah Kembali</button>',
  );
  return $status[$value];
}

 ?>

<table class="table" border="1px">
 <thead>
   <tr>
     <th scope="col">#</th>
     <th scope="col">Nama</th>
     <th scope="col">Ukuran</th>
     <th scope="col">Jumlah</th>
     <th scope="col">Harga / 3 hari</th>
     <th scope="col">Total</th>
   </tr>
 </thead>
 <tbody>
 @foreach($data['busana'] as $i =>$item)
   <tr>
     <th scope="row">{{ $i+1 }}</th>
     <td>{{ $item->getBusana->nama }}</td>
     <td>{{ $item->getBusana->size }}</td>
     <td>{{ $item->jumlah }}</td>
     <td>{{ rupiah($item->getBusana->harga) }}</td>
     <td>{{ rupiah($item->getBusana->harga*$item->jumlah) }}</td>
   </tr>
 </tbody>
 @endforeach
</table>
<br>

Dengan Total: {{ rupiah($data['sewa']->sub_total) }}
<br>
<br>
Terimakasih.