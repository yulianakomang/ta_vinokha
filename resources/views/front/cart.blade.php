@extends('front.app') @section('content')
<script>
</script>
<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area bg-img" style="background-image: url({{ asset('front/img/bg-img/breadcumb.jpg')}});">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2>Keranjang</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Checkout Area Start ##### -->
<div class="checkout_area section-padding-80">
    <div class="container">
        @if($errors->any())
                <div class="alert alert-danger text-center" role="alert">
                    {{$errors->first()}}
                </div>
                @endif
        <div class="row">

            <div class="col-12 col-md-6">
                @if ($cart==!null) 
                <div class="cart-page-heading mb-30">
                    <h5>Keranjang</h5>
                </div>
                
                @foreach ($cart as $item)

                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{ asset('uploads/'.$item['foto'])}}" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <a href="{{ url('cart/delete/'.$item['id'])}}" type="button" class="close mr-2 mt-1" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </a>
                            <div class="card-body">
                                <span> 
                                    Busana Tari
                                </span>
                                <a href="{{ url('detail/'.$item['id'])}}">
                                    <h6>{{$item['nama']}}</h6>
                                </a>
                                <p class="product-price">{{$item['harga']}} / 3 Hari
                                    <br>
                                    <form method="post" action="{{ route('public-addToCart', ['id' => $item['id']])}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-3">
                                                <input name="jumlah" value="{{$item['jumlah']}}" type="number" class="form-control mb-3 form-control-sm" id="jumlah" value="" min="1" max="{{$item['stok']}}">
                                            </div>
                                            <div class="col-3">
                                                <input type="submit" class="btn btn-light btn-sm" value="Update pesanan">
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-12 col-md-6">
                    <div class="alert alert-light" role="alert">
                        Belum ada barang
                    </div>
                </div>
                @endif
                
                <div class="checkout_details_area mt-50 clearfix">

                    <div class="cart-page-heading mb-30">
                        <h5>Detail pembayaran</h5>
                    </div>
                    @if ($user==!null)
                    <div class="card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <i class="fa fa-user" style="font-size: 164px;margin-left: 35px;"></i>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{$user['nama']}}</h5>
                                    <p class="card-text">
                                        Alamat : {{$user['alamat']}}
                                        <br> No Telp : {{$user['no_telp']}}
                                        <br> Email : {{$user['email']}}
                                        <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else

                    @if (Route::current()->getName() == 'public-cart' && $user==null )
                    <div class="card mb-4">
                        <div class="card-body">
                            <h6>Apakah anda sudah punya akun? silahkan</h6>
                            <a class="btn essence-btn" href="{{ url('public-login') }}">Login</a>
                        </div>
                    </div>
                     <h6>Atau silahkan register disini</h6>
                        <form method="POST" action="{{ route('register-store') }}">
                        @csrf
                        <input type="hidden" value="3" name="level">
                        <input type="hidden" value="avatar.png" name="user_image">
                        <div class="input-group mb-3">
                            <input type="text" name="nama" class="form-control" placeholder="Nama">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="alamat" class="form-control" placeholder="Alamat">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="no_ktp" class="form-control" placeholder="Nomor KTP">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="no_telp" class="form-control" placeholder="No Telepon">
                        </div>
                        <div class="input-group mb-3">
                            <input type="email" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>

                        <div class="row">
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn essence-btn">REGISTER</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                    @elseif(Route::current()->getName() == 'public-login' )
                    <div class="card mb-4">
                        <div class="card-body">
                            <h6>Apakah anda belum punya akun? silahkan</h6>
                            <a class="btn essence-btn" href="{{ url('cart') }}">Register</a>
                        </div>
                    </div>
                     <h6>Atau silahkan Login</h6>

                        <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                              
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                             
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn essence-btn">LOGIN</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                    @endif
                    @endif
                   
                    
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-5">
                <div class="order-details-confirmation">

                    <div class="cart-page-heading">
                        <h5>Detail pesanan</h5>
                    </div>

                    <ul class="order-details-form mb-4">
                        <li><span>Produk</span> <span>Total</span></li>
                        @if ($cart==!null) @foreach ($cart as $item)
                        <li>
                            <span>
                                    {{$item['nama']}}
                                    <br>
                                    Rp. {{$item['harga']}} &#x2715; {{$item['jumlah']}}
                                </span>
                            <span>
                                    Rp. {{$item['total']}}
                                </span>
                        </li>
                        @endforeach @endif
                        <li><span>Total</span> <span>Rp.<script type="text/javascript">
                                document.write(total)
                            </script></span></li>
                    </ul>
                    <form class="needs-validation" action="{{ route('public-payment')}}" novalidate>

                        <input type="hidden" name="total" id="total" value="Swag">
                        <script type="text/javascript">
        @if ($cart==!null)
        var total = @foreach ($cart as $item) {{$item['total']}} + @endforeach 0;
        @else
        var total = 0;
        @endif

        document.getElementById("total").value = total;
        
    </script>
                    <div class="form-group mb-3">
                        <label>Tanggal Pinjam</label>
                        <input type="date" name="tgl_pinjam" class="form-control " required min="{{ date('Y-m-d') }}">
                        
                        <div class="invalid-feedback">
                            Masukan tanggal pinjam
                          </div>
                      </div>
                      

                    <input type="submit" class="btn essence-btn" value="Bayar">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ##### Checkout Area End ##### -->

@endsection