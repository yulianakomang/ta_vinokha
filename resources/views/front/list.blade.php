@extends('front.app')
@section('content')
<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area bg-img" style="background-image: url({{ asset('front/img/bg-img/breadcumb.jpg')}});">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2>Daftar Produk</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Shop Grid Area Start ##### -->
<section class="shop_grid_area section-padding-80">
    <div class="container">
        <div class="row">


            <div class="col-12">
                <div class="shop_grid_product_area">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-topbar d-flex align-items-center justify-content-between">
                                <!-- Total Products -->
                                <div class="total-products">
                                    <p><span>{{$jml_data}}</span> Produk ditemukan</p>
                                </div>
                                <!-- Sorting -->
                                <div class="product-sorting d-flex">
                                   {{--  <p>Sort by:</p>
                                    <form action="#" method="get">
                                        <select name="select" id="sortByselect">
                                            <option value="value">Price: $$ - $</option>
                                            <option value="value">Price: $ - $$</option>
                                        </select>
                                        <input type="submit" class="d-none" value="">
                                    </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <!-- Single Product -->
                        @foreach ($data as $item)
                        <div class="col-12 col-sm-6 col-lg-3">

                            <div class="single-product-wrapper">
                                <!-- Product Image -->
                                <div class="product-img">
                                    <img src="{{asset('uploads/'.$item->foto )}}" alt="">
                                    <!-- Hover Thumb -->
                                    <!-- <img class="hover-img" src="img/product-img/product-2.jpg" alt=""> -->
                                    <!-- Favourite -->
                                    
                                </div>
                                <!-- Product Description -->
                                <div class="product-description">
                                    <span>
                                        Busana Tari
                                    </span>
                                    <a href="{{ url('detail/'.$item->id)}}">
                                        <h6>{{$item->nama}}</h6>
                                    </a>
                                    <p class="product-price">Rp.{{$item->harga}} / 3 Hari</p>

                                    <!-- Hover Content -->
                                    <div class="hover-content">
                                        <!-- Add to Cart -->
                                        <div class="add-to-cart-btn">
                                            <form class="cart-form clearfix" method="post" action="{{ route('public-addToCart', ['id' => $item->id])}}">
                                                @csrf
                                                 <input name="jumlah" type="hidden" class="form-control mb-3" value="1"> 
                                                <input type="submit" class="btn essence-btn" value="Masukan Keranjang" style="padding: 0 20px">
                                            </form>
                                        </div>
                                    </div>
                                    <div style="clear: both;padding: 10px"></div>
                                </div>
                            </div>

                        </div>
                        @endforeach
                        

                    </div>
                </div>
                <!-- Pagination -->
                <nav aria-label="navigation">
                    @include('front.pagination.default', ['paginator' => $data])
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- ##### Shop Grid Area End ##### -->
@endsection