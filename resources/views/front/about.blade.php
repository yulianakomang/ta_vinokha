@extends('front.app')
@section('content')
<!-- ##### Blog Wrapper Area Start ##### -->
<div class="single-blog-wrapper">

    <!-- Single Blog Post Thumb -->
    <div class="single-blog-post-thumb">
        <img src="{{ asset('front/img/bg-img/bg-8.jpeg')}}" alt="">
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="regular-page-content-wrapper section-padding-80">
                    <div class="regular-page-text">
                        <h2>Tentang Kami</h2>
                        <p align="justify">Sanggar Tari Dewi Saraswati adalah sebuah organisasi yang berdiri sejak tanggal 23 Oktober 2003 yang dibangun untuk mewadahi kreativitas seni tari warga masyarakat. Sanggar Tari ini telah didirikan oleh Ni Wayan Sriartini S.Sn., Sanggar Tari Dewi Saraswati tidak hanya menyediakan tempat untuk mewadahi kreativitas masyarakat sebagai penari saja, namun sanggar ini juga menyediakan jasa penyewaan berbagai macam busana tari tradisional Bali.</p>

                        <blockquote>
                            <h6><i class="fa fa-quote-left" aria-hidden="true"></i> Sanggar kami juga menyediakan busana tari tradisional Lombok, tari kreasi serta kami juga menyediakan busana untuk prawedd dan busana pengantin Bali.</h6>
                            <span>Ni Wayan Sriartini S.Sn.,</span>
                        </blockquote>

                        <p>Sanggar Dewi Saraswati berhasil menciptakan lebih dari 10 tari kreasi dan telah menciptakan ribuan penari hebat dari usia 7 sampai dengan 20 tahun.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Blog Wrapper Area End ##### -->
@endsection