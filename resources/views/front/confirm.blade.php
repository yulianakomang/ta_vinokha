@extends('front.app')
@section('content')
<div class="breadcumb_area bg-img" style="background-image: url({{ asset('front/img/bg-img/breadcumb.jpg')}});">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2>Konfirmasi Pembayaran</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mt-5 mb-5">
        <div class="col-12 col-md-8 offset-0 offset-md-2">
            <div class="card">
                {{-- <h5 class="card-header">Pembayaran melalui Transfer Bank</h5> --}}
                <div class="card-body">

                    <h4 class="">Silahkan Menyelesaikan pembayaran</h4>
                    
                    <h6 class="">Silahkan tranfer Uang Muka (50%) sebesar : Rp.{{$data['sub_total']/2}}</h6>
                    <p>*Sisa pembayaran saat pengambilan barang.</p>
                    Ke bank berikut :
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col" width="20%">Bank</th>
                          <th scope="col">No. Rek</th>
                          <th scope="col">Atas Nama</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>
                              <img width="100%" src="https://upload.wikimedia.org/wikipedia/id/thumb/e/e0/BCA_logo.svg/472px-BCA_logo.svg.png" alt="">
                          </td>
                          <td>11234556</td>
                          <td>dewisaraswati</td>
                        </tr>
                      </tbody>
                    </table>

                    <form method="post" action="{{ route('public-confirm')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_user" value="{{$data['id_user']}}"> 
                        <input type="hidden" name="kode" value="{{$data['kode']}}"> 
                        <input type="hidden" name="tgl_pinjam" value="{{$data['tgl_pinjam']}}"> 
                        <input type="hidden" name="tgl_pengembalian" value="{{$data['tgl_pengembalian']}}"> 
                        <input type="hidden" name="sub_total" value="{{$data['sub_total']}}"> 
                        <input type="hidden" name="status_penyewaan" value="{{$data['status_penyewaan']}}"> 
                        <input type="hidden" name="online" value="{{$data['online']}}"> 
                        <div class="form-group mb-3">
                            <label>Upload Bukti Bayar</label>
                            <input type="file" class="form-control-file" name="image" id="exampleFormControlFile1">
                        </div>
                        <input type="submit" class="btn essence-btn" name="">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection