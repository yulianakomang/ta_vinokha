@extends('front.app')
@section('content')
<section class="welcome_area bg-img background-overlay" style="background-image: url({{ asset('front/img/bg-img/bg-1.jpg')}});">
	<div class="container h-100">
		<div class="row h-100 align-items-center">
			<div class="col-12">
				<div class="hero-content">
					<h6 style="text-shadow: 2px 2px #000000; color:#FFFFFF;">NEW</h6>
					<h2 style="text-shadow: 2px 2px #000000; color:#FFFFFF;" class="font-indonesia">koleksi terbaru</h2>
					<a href="{{route('public-list')}}" class="btn essence-btnn">tampilkan koleksi</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="new_arrivals_area section-padding-80 clearfix">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-heading text-center">
					<h2>Product</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="popular-products-slides owl-carousel">

					<!-- Single Product -->
					@foreach ($data as $item)
					<div class="single-product-wrapper">
						<!-- Product Image -->
						<div class="product-img">
							<img src="{{asset('uploads/'.$item->foto )}}" alt="">
							<!-- Hover Thumb -->
							<!-- <img class="hover-img" src="img/product-img/product-2.jpg" alt=""> -->
							<!-- Favourite -->
							
						</div>
						<!-- Product Description -->
						<div class="product-description">
							<span>
								Busana Tari
							</span>
							<a href="{{ url('detail/'.$item->id)}}">
								<h6>{{$item->nama}}</h6>
							</a>
							<p class="product-price">{{$item->harga}}</p>

							<!-- Hover Content -->
							<div class="hover-content">
								<!-- Add to Cart -->
								<div class="add-to-cart-btn">
									<form class="cart-form clearfix" method="post" action="{{ route('public-addToCart', ['id' => $item->id])}}">
										@csrf
										<input name="jumlah" type="hidden" class="form-control mb-3" value="1"> 
										<input type="submit" class="btn essence-btn" value="Masukan Keranjang" style="padding: 0 20px">
									</form>
								</div>
							</div>
							<div style="clear: both;padding: 10px"></div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<p class="text-center">
		<a href="{{ url('/list') }}" class="btn essence-btn">TAMPILKAN SEMUA KOLEKSI</a>
	</p>
</section>

@endsection