<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Sanggar Dewi Saraswati</title>

    <!-- Favicon  -->
    <link rel="icon" href="{{ asset('front/img/core-img/favicon.ico')}}">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{ asset('front/css/core-style.css')}}">
    <link rel="stylesheet" href="{{ asset('front/style.css')}}">
    <style>
        .active a{
            color: white !important;
        }
        .single-product-wrapper{
            box-shadow: 11px 9px 21px -2px rgba(0,0,0,0.16);
        }
        .product-description{
            padding-left: 10px;
        }
    </style>
    <script type="text/javascript">
        @if ($cart==!null)
        var total = @foreach ($cart as $item) {{$item['total']}} + @endforeach 0;
        @else
        var total = 0;
        @endif
        document.getElementById("total").value = total;
    </script>

</head>

<body>
    <!-- ##### Header Area Start ##### -->
    <header class="header_area">
        <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
            <!-- Classy Menu -->
            <nav class="classy-navbar" id="essenceNav">
                <!-- Logo -->
                {{-- <a class="nav-brand" href="index.html"><img src="{{ asset('front/img/core-img/logo.png')}}" alt=""></a> --}}
                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>
                <!-- Menu -->
                <div class="classy-menu">
                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>
                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul>
                            <li><a href="{{ route('public-index')}}">HOME</a></li>
                            <li><a href="{{ route('public-list')}}">Busana Tari</a></li>
                            <li><a href="{{ route('public-about')}}">Tentang Saya</a></li>
                            <li><a href="{{ route('public-contact')}}">Kontak</a></li>
                        </ul>
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>

            <!-- Header Meta Data -->
            <div class="header-meta d-flex clearfix justify-content-end">
                <!-- Search Area -->

                <!-- Favourite Area -->

                <!-- User Login Info -->
                <div class="user-login-info dropdown">
                    <a href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><img src="{{ asset('front/img/core-img/user.svg')}}" alt=""></a>
                    <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                        @if ($user==!null)
                        <h6 class="dropdown-item ">Hai, {{ $user['nama'] }}</h6>
                        <li class="dropdown-item ">
                            <a style="border: none;line-height: 30px;" href="{{ url('logout')}}">Logout</a>
                        </li>
                        @else
                        <li class="dropdown-item ">
                            <a style="border: none;line-height: 30px;" href="{{ url('login')}}">Login</a>
                        </li>
                        <li class="dropdown-item ">
                            <a style="border: none;line-height: 30px;" href="{{ url('register')}}">Register</a>
                        </li>
                        @endif
                        
                    </div>
                </div>
                <!-- Cart Area -->
                <div class="cart-area">
                    <a href="#" id="essenceCartBtn"><img src="{{ asset('front/img/core-img/bag.svg')}}" alt="">
                        <span>{{$cart_jml}}</span></a>
                    </div>
                </div>

            </div>
        </header>
        <!-- ##### Header Area End ##### -->

        <!-- ##### Right Side Cart Area ##### -->
        <div class="cart-bg-overlay"></div>

        <div class="right-side-cart-area">

            <!-- Cart Button -->
            <div class="cart-button">
                <a href="#" id="rightSideCart"><img src="{{ asset('front/img/core-img/bag.svg')}}" alt="">
                    <span>{{$cart_jml}}</span></a>
                </div>

                <div class="cart-content d-flex">
                    <!-- Cart Summary -->
                    <div class="cart-amount-summary">

                        <h2>Keranjang</h2>
                        @if ($cart==!null)
                        @foreach ($cart as $item)
                        <div class="card mb-3">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src="{{ asset('uploads/'.$item['foto'])}}" class="card-img" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <a href="{{ url('cart/delete/'.$item['id'])}}" type="button" class="close mr-2 mt-1" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </a>
                                    <div class="card-body">
                                        <span>
                                            Busana Tari
                                        </span>
                                        <a href="{{ url('cart/'.$item['id'])}}">
                                            <h6>{{$item['nama']}}</h6>
                                        </a>
                                        <p class="product-price">
                                            {{$item['harga']}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        
                        <ul class="summary-table">
                            <li><span>total:</span> <span><script type="text/javascript">
                                document.write(total)
                            </script></span></li>
                        </ul>
                        <div class="checkout-btn mt-100">
                            <a href="{{url('cart')}}" class="btn essence-btn">Keranjang</a>
                            <a href="{{url('checkout')}}" class="btn essence-btn">Bayar</a>
                        </div>
                    </div>
                </div>
            </div>
            @yield('content')
            <footer class="footer_area clearfix">
                <div class="container">
                    <div class="row">
                        <!-- Single Widget Area -->
                        <div class="col-12 col-md-6">
                            <div class="single_widget_area d-flex mb-30">
                                <!-- Logo -->
                                {{-- <div class="footer-logo mr-50">
                                    <a href="#"><img src="{{ asset('front/img/core-img/logo2.png')}}" alt=""></a>
                                </div> --}}
                                <!-- Footer Menu -->
                                <div class="footer_menu">
                                    <ul>
                                     <li><a href="{{ route('public-index')}}">Home</a></li>
                                     <li><a href="{{ route('public-list')}}">Busana Tari</a></li>
                                     <li><a href="{{ route('public-about')}}">Tentang Saya</a></li>
                                     <li><a href="{{ route('public-contact')}}">Kontak</a></li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                     <!-- Single Widget Area -->
                     <div class="col-12 col-md-6">
                        <div class="single_widget_area">
                            <div class="footer_social_area">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i
                                    class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://www.instagram.com/st.dewisaraswati/?hl=id" data-toggle="tooltip" data-placement="top" title="Instagram"><i
                                        class="fa fa-instagram" aria-hidden="true"></i></a>
                                        <a href="https://wa.me/6281805288804" data-toggle="tooltip" data-placement="top" title="Whatsapp"><i
                                            class="fa fa-whatsapp" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row align-items-end">
                                        <!-- Single Widget Area -->

                                        <!-- Single Widget Area -->

                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-12 text-center">
                                            <p>
                                                Copyright &copy;<script>
                                                    document.write(new Date().getFullYear());
                                                </script> All rights reserved.
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </footer>
                            <!-- ##### Footer Area End ##### -->

                            <!-- jQuery (Necessary for All JavaScript Plugins) -->
                            <script src="{{ asset('front/js/jquery/jquery-2.2.4.min.js') }}"></script>
                            <!-- Popper js -->
                            <script src="{{ asset('front/js/popper.min.js') }}"></script>
                            <!-- Bootstrap js -->
                            <script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
                            <!-- Plugins js -->
                            <script src="{{ asset('front/js/plugins.js') }}"></script>
                            <!-- Classy Nav js -->
                            <script src="{{ asset('front/js/classy-nav.min.js') }}"></script>
                            <!-- Active js -->
                            <script src="{{ asset('front/js/active.js') }}"></script>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
$(document).on("keypress","[type='number']",function (evt) {
    evt.preventDefault();
});
</script>

                        </body>

                        </html>