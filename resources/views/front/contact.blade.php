@extends('front.app')
@section('content')
<div class="contact-area d-flex align-items-center container">

    <div class="google-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3945.180688408792!2d116.12877694872861!3d-8.578618993803794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dcdc0ac163466a3%3A0xf132ccff7f576854!2sSanggar%20Tari%20Dewi%20Saraswati!5e0!3m2!1sid!2sid!4v1578975786711!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>

    <div class="contact-info">
        <h2>Silahkan datang dan berkunjung ke Sanggar Dewi Saraswati</h2>
        <p>Alamat dan Nomer Hp sudah tertera dibawah</p>

        <div class="contact-address mt-50">
            <p><span>Alamat:</span> Jl. Ade Irma Suryani, Cakranegara Utara, Kec. Cakranegara, Kota Mataram, Nusa Tenggara Bar. 83239</p>
            <p><span>No Hp:</span> 0818-0528-8804</p>
            <p><a href="mailto:contact@essence.com">contact@essence.com</a></p>
        </div>
    </div>

</div>
@endsection