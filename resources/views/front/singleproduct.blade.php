@extends('front.app')
@section('content')
<!-- ##### Single Product Details Area Start ##### -->
<section class="single_product_details_area d-flex align-items-center container">

    <!-- Single Product Thumb -->
    @foreach($data as $item)
    <div class="single_product_thumb clearfix">
        <img src="{{asset('uploads/'.$item->foto )}}" alt="">
        
    </div>

    <!-- Single Product Description -->
    <div class="single_product_desc clearfix">
        <span>
            Busana Tari
        </span>
        <a href="#">
            <h2>{{$item->nama}} </h2>
        </a>
        <p class="product-price">{{$item->harga}} / 3 HARI</p>

        <!-- Form -->
        <form class="cart-form clearfix" method="post" action="{{ route('public-addToCart', ['id' => $item->id])}}">
            @csrf
            <input type="hidden" name="id" value="{{$item->id}}">
            <div class="row">
                <div class="col-3">
                   <label for="jumlah">Jumlah</label>
                   <input name="jumlah" type="number" class="form-control mb-3" id="jumlah" value="1"> 
               </div>
           </div>
           
           <div class="cart-fav-box d-flex align-items-center">
            <!-- Cart -->
            <input type="submit" class="btn essence-btn" value="Masukan Keranjang">
            <!-- Favourite -->
           
        </div>
    </form>
</div>
@endforeach

<div class="album-foto">
    <h3>Album Foto</h3>
    <div style="clear: both;padding: 20px"></div>
    <div class="row">
        @forelse($album as $i)
        <div class="col-md-3">
          <img src="{{ asset('uploads/'.$i->foto ) }}" class="w-100">
        </div>
        @empty
            <p>Belum ada Album.</p>
        @endforelse
    </div>
</div>

<div style="clear: both;padding: 20px"></div>

</section>
<!-- ##### Single Product Details Area End ##### -->
@endsection