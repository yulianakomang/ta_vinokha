@extends('front.app')
@section('content')
<!-- ##### Breadcumb Area Start ##### -->

<div class="breadcumb_area bg-img" style="background-image: url({{ asset('front/img/bg-img/breadcumb.jpg')}});">
	<div class="container h-100">
		<div class="row h-100 align-items-center">
			<div class="col-12">
				<div class="page-title text-center">
					<h2>Checkout</h2>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Checkout Area Start ##### -->
<div class="checkout_area section-padding-80">
	<div class="container">
		<div class="row">

			<div class="col-12 col-md-6">
				<div class="checkout_details_area mt-50 clearfix">

					<div class="cart-page-heading mb-30">
						<h5>Billing Detail</h5>
					</div>
					@if ($user==!null)
					<div class="card mb-3">
						<div class="row no-gutters">
							<div class="col-md-4">
								<img src="{{ asset('uploads/'.$user['user_image'])}}" class="card-img" alt="...">
							</div>
							<div class="col-md-8">
								<div class="card-body">
									<h5 class="card-title">{{$user['nama']}}</h5>
									<p class="card-text">
										Alamat : {{$user['alamat']}} <br>
										No Telp : {{$user['no_telp']}} <br>
										Email   : {{$user['email']}} <br>
									</p>
								</div>
							</div>
						</div>
					</div>
					@else
					<div class="card mb-4">
						<div class="card-body">
							<h6>Apakah anda sudah punya akun? silahkan</h6>
							<a class="btn essence-btn" href="{{ url('login') }}">Login</a>
						</div>
					</div>
					<h6>Atau silahkan register disini</h6>
					<form method="POST" action="{{ route('register-store') }}">
						@csrf
						<input type="hidden" value="3" name="level">
						<input type="hidden" value="avatar.png" name="user_image">
						<div class="input-group mb-3">
							<input type="text" name="nama" class="form-control" placeholder="Nama">
						</div>
						<div class="input-group mb-3">
							<input type="text" name="alamat" class="form-control" placeholder="Alamat">
						</div>
						<div class="input-group mb-3">
							<input type="text" name="no_ktp" class="form-control" placeholder="Nomor KTP">
						</div>
						<div class="input-group mb-3">
							<input type="text" name="no_telp" class="form-control" placeholder="No Telepon">
						</div>
						<div class="input-group mb-3">
							<input type="email" name="email" class="form-control" placeholder="Email">
						</div>
						<div class="input-group mb-3">
							<input type="password" name="password" class="form-control" placeholder="Password">
						</div>

						<div class="row">

							<!-- /.col -->
							<div class="col-4">
								<button type="submit" class="btn essence-btn">REGISTER</button>
							</div>

							<!-- /.col -->
						</div>

					</form>
					@endif

					<form action="#" method="post">
						{{-- <div class="row">
							<div class="col-md-12 mb-3">
								<label for="first_name">First Name <span>*</span></label>
								<input type="text" class="form-control" id="first_name" value="" required>
							</div>
							<div class="col-12 mb-3">
								<label for="street_address">Address <span>*</span></label>
								<input type="text" class="form-control mb-3" id="street_address" value="">
							</div>
							<div class="col-12 mb-4">
								<label for="email_address">Email Address <span>*</span></label>
								<input type="email" class="form-control" id="email_address" value="">
							</div>
						</div> --}}
					</form>
				</div>
			</div>

			<div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
				<div class="order-details-confirmation">

                    <div class="cart-page-heading">
                        <h5>Detail order</h5>
                    </div>

                    <ul class="order-details-form mb-4">
                        <li><span>Product</span> <span>Total</span></li>
                        @if ($cart==!null) @foreach ($cart as $item)
                        <li>
                            <span>
                        {{$item['nama']}}
                        <br>
                        Rp. {{$item['harga']}} &#x2715; {{$item['jumlah']}}
                        </span>
                            <span>
                        Rp. {{$item['total']}}
                        </span>
                        </li>
                        @endforeach @endif
                        <li><span>Total</span> <span>Rp.<script type="text/javascript">
                        document.write(total)
                    </script></span></li>
                    </ul>

                    <a href="{{ route('public-checkout')}}" class="btn essence-btn">Checkout</a>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- ##### Checkout Area End ##### -->
@endsection