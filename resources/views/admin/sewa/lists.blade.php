@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
  <style>
    /* Important part */
.modal-dialog-custom{
    overflow-y: initial !important
}
.modal-body-custom{
    height: 450px;
    overflow-y: auto;
}
  </style>
@endsection

@section('content')

<?php 

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}

function status($value='')
{
  $status = array(
    '<button class="btn btn-danger">Belum Diambil</button>',
    '<button class="btn btn-primary">Sudah Diambil</button>',
    '<button class="btn btn-success">Sudah Kembali</button>',
  );
  return $status[$value];
}

function get_denda($tgl_tenpo='',$harga='')
{
  $tgl_sekarang = date('Y-m-d');
  $tgl_seharusnya = date('Y-m-d',strtotime($tgl_tenpo));
  $start_date = new \DateTime($tgl_sekarang);
  $end_date = new \DateTime($tgl_seharusnya);
  $interval = $start_date->diff($end_date);
  if ($start_date > $end_date) {
    return array(($harga/2)*$interval->days,$interval->days);

  }
}


 ?>

 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Daftar Sewa</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Daftar Sewa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <a href="{{ route('sewa-add') }}" class="btn btn-success mb-4"><i class="fa fa-plus"></i>  Tambah</a>

              <table class="table table-bordered table-striped load_datatables">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Kode</th>
                  <th>Peminjam</th>
                  <th>TGL Pinjam</th>
                  <th>TGL Kembali</th>
                  <th>Total</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($result['lists'] as $i => $item)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td><a href="{{ route('sewa-detail',['id'=>$item->id]) }}" class="btn btn-primary">{{ $item->kode }}</a></td>
                  <td>{{ $item->getUser->nama }}</td>
                  <td>{{ tgl_indo($item->tgl_pinjam) }}</td>
                  <td>{{ tgl_indo($item->tgl_pengembalian) }}</td>
                  <td>{{ rupiah($item->sub_total) }} 
                    @if($item->status_penyewaan == 1 || $item->status_penyewaan == 2)
                    <br>Denda: {{ rupiah(get_denda($item->tgl_pengembalian,$item->sub_total)[0]) }}

                    ({{ get_denda($item->tgl_pengembalian,$item->sub_total)[1] }}x)

                    <br>Total: {{ rupiah($item->sub_total + (get_denda($item->tgl_pengembalian,$item->sub_total))[0]) }}
                    @endif
                  </td>
                  <td>{!! status($item->status_penyewaan) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary">Opsi</button>
                      <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                        <span class="caret"></span>
                      </button>
                      <div class="dropdown-menu">
                        @if($item->status_penyewaan != 0 && $item->status_penyewaan != 2 && $item->status_penyewaan != 1)
                        <a class="dropdown-item" href="{{ route('sewa-set1',['id'=>$item->id]) }}">Belum Diambil</a>
                        @endif
                        @if($item->status_penyewaan != 1 && $item->status_penyewaan != 2)
                        <a class="dropdown-item confirm_sudahambil" data-id="{{ $item->id }}" data-url="{{ route('sewa-get',['id'=>$item->id]) }}" data-url-full="{{ route('sewa-set2',['id'=>$item->id]) }}" href="#" data-toggle="modal" data-target="#pelunasan">Sudah Diambil</a>
                        @endif
                        @if($item->status_penyewaan != 2 && $item->status_penyewaan != 0)
                        <a class="dropdown-item confirm_kembali" data-id="{{ $item->id }}" data-url="{{ route('sewa-get-kembali',['id'=>$item->id]) }}" data-url-full="{{ route('sewa-set3',['id'=>$item->id]) }}" href="#" data-toggle="modal" data-target="#pengembalian">Sudah Kembali</a>
                        @endif
                        @if($item->status_penyewaan == 0)
                        <a class="dropdown-item" href="{{ route('sewa-edit',['id'=>$item->id]) }}">Edit</a>
                        <a class="dropdown-item" href="{{ route('sewa-delete',['id'=> $item->id]) }}">Delete</a>
                        @endif
                      </div>
                    </div>                  
                  </td>
                  
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



<!-- Modal -->
<div class="modal fade" id="pelunasan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pengambilan & Pelunasan Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Kode</label>
          <input type="text" name="kode" class="form-control form-kode" readonly>
        </div>
        <div class="form-group">
          <label>Total Harga</label>
          <input type="text" name="total" class="form-control form-total" readonly>
        </div>
        <div class="form-group">
          <label>Uang Muka</label>
          <input type="text" name="dp" class="form-control form-dp" readonly>
        </div>
        <div class="form-group">
          <label>Total sisa harus Dibayar</label>
          <input type="text" name="sisa" class="form-control form-sisa" readonly>
        </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <a href="" class="btn btn-primary aksi_ambil">Diambil & Lunas</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="pengembalian" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pengembalian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Kode</label>
          <input type="text" name="kode" class="form-control form-kode" readonly>
        </div>
        <div class="form-group">
          <label>Tgl Sewa</label>
          <input type="text" name="tgl_sewa" class="form-control form-tgl_sewa" readonly>
        </div>
        <div class="form-group">
          <label>Tgl seharusnya kembali</label>
          <input type="text" name="kembali_tempo" class="form-control form-kembali_tempo" readonly>
        </div>
        <div class="form-group">
          <label>Tgl kembali</label>
          <input type="text" name="kembali" class="form-control form-kembali" readonly value="{{ date('Y-m-d') }}">
        </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <a href="" class="btn btn-primary aksi_kembali">Sudah Dikembalikan</a>
      </div>
    </div>
  </div>
</div>


    @endsection

    @section('js')

<script src="{{ asset('/') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Summernote -->
<script src="{{ asset('/') }}plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">

  $(document).on('click','.confirm_sudahambil',function(){
    var id = $(this).data('id');
    var url = $(this).data('url');
    var url_full = $(this).data('url-full');

    $('.aksi_ambil').attr('href',url_full);

    $.get(url,function(res){
      var result = $.parseJSON(JSON.stringify(res));
      $.each(result, function(i,val){
        if(i === 'content'){
          $('#EditModal').find('.note-editable').html(val);
        }else{
          $('.form-'+i).val(val);
        }
      });
    });
  });

  $(document).on('click','.confirm_kembali',function(){
    var id = $(this).data('id');
    var url = $(this).data('url');
    var url_full = $(this).data('url-full');

    $('.aksi_kembali').attr('href',url_full);

    $.get(url,function(res){
      var result = $.parseJSON(JSON.stringify(res));
      $.each(result, function(i,val){
        if(i === 'content'){
          $('#EditModal').find('.note-editable').html(val);
        }else{
          $('.form-'+i).val(val);
        }
      });
    });
  });
  
  $('.load_datatables').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  $('.load_editor').summernote();

  
</script>
    @endsection