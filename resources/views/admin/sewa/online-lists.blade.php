@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
  <style>
    /* Important part */
.modal-dialog-custom{
    overflow-y: initial !important
}
.modal-body-custom{
    height: 450px;
    overflow-y: auto;
}
  </style>
@endsection

@section('content')

<?php 

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}

function status($value='')
{
  $status = array(
    '<button class="btn btn-danger">Belum Diambil</button>',
    '<button class="btn btn-primary">Sudah Diambil</button>',
    '<button class="btn btn-success">Sudah Kembali</button>',
  );
  return $status[$value];
}

 ?>

 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Online Booking</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Online Booking</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">

              <table class="table table-bordered table-striped load_datatables">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Kode</th>
                  <th>Peminjam</th>
                  <th>TGL Pinjam</th>
                  <th>TGL Kembali</th>
                  <th>Total</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($result['lists'] as $i => $item)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td><a href="{{ route('sewa-detail',['id'=>$item->id]) }}" class="btn btn-primary">{{ $item->kode }}</a></td>
                  <td>{{ $item->getUser->nama }}</td>
                  <td>{{ tgl_indo($item->tgl_pinjam) }}</td>
                  <td>{{ tgl_indo($item->tgl_pengembalian) }}</td>
                  <td>{{ rupiah($item->sub_total) }}</td>
                  <td>{!! status($item->status_penyewaan) !!}</td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary">Opsi</button>
                      <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                        <span class="caret"></span>
                      </button>
                      <div class="dropdown-menu">
                       
                        <a class="dropdown-item" href="{{ url('uploads/'.$item->bukti_tf) }}" target="_blank">Lihat Bukti Transfer</a>
                        <a class="dropdown-item" href="{{ route('sewa-online-terima',['id'=>$item->id]) }}">Setujui/Terima</a>
                      </div>
                    </div>                  
                  </td>
                  
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



    @endsection

    @section('js')

<script src="{{ asset('/') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Summernote -->
<script src="{{ asset('/') }}plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">

  $(document).on('click','.confirm_sudahambil',function(){
    var id = $(this).data('id');
    var url = $(this).data('url');
    var url_full = $(this).data('url-full');

    $('.aksi_ambil').attr('href',url_full);

    $.get(url,function(res){
      var result = $.parseJSON(JSON.stringify(res));
      $.each(result, function(i,val){
        if(i === 'content'){
          $('#EditModal').find('.note-editable').html(val);
        }else{
          $('.form-'+i).val(val);
        }
      });
    });
  });
  
  $('.load_datatables').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  $('.load_editor').summernote();

  
</script>
    @endsection