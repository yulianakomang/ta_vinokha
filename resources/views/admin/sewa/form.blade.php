@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
  <style>
    /* Important part */
.modal-dialog-custom{
    overflow-y: initial !important
}
.modal-body-custom{
    height: 450px;
    overflow-y: auto;
}
  </style>
@endsection

@section('content')

 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Sewa Busana</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Sewa Busana</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <form action="{{ route('sewa-store') }}" method="post">
                @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Kode</label>
                    <input type="text" name="kode" class="form-control" readonly value="SW-<?= rand(11111,99999) ?>">
                  </div>
                  <div class="form-group">
                    <label>Peminjam</label>
                    <select name="id_user" id="" class="form-control">
                      <option value="">-- pilih --</option>
                      @foreach($result['users'] as $user)
                      <option value="{{ $user->id }}">{{ $user->nama }}</option>
                      @endforeach
                    </select>
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>TGL Pinjam</label>
                    <input type="date" name="tgl_pinjam" class="form-control" value="{{ date('Y-m-d') }}">
                  </div>
                  <div class="form-group">
                    <label>TGL Pengembalian</label>
                    <input type="date" name="tgl_pengembalian" class="form-control" value="{{ date('Y-m-d',strtotime('+ 3 days')) }}">
                  </div>
                </div>

                <div class="col-md-12 load_produk">
                  <!-- List Produk -->
                  <div class="card item-produk">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Pilih Busana</label>
                          <select name="id_busana[]" id="" class="form-control itemBselect-1">
                            <option value="">-- pilih --</option>
                            @foreach($result['busana'] as $busana)
                            <option value="{{ $busana->id }}" data-harga="{{ $busana->harga }}" data-stok="{{ $busana->stok }}">{{ $busana->kode }} - {{ $busana->nama }} - Size:{{ $busana->size }}</option>
                            @endforeach
                          </select>
                        </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Jumlah (Stok: <span class="load_stok-1"></span>) </label>
                            <input type="number" name="jumlah[]" min="1" value="1" class="form-control itemBjumlah-1">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Harga</label>
                            <input type="text" class="form-control itemBharga-1" readonly>
                            <input type="hidden" class="form-control itemBhargasatuan-1">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  

                </div>
                <div class="col-md-3 offset-md-9">
                  <div class="form-group">
                    <label>Total</label>
                    <input type="text" name="sub_total" class="form-control itemBharga-1" readonly>
                  </div>
                  <div class="form-group">
                    <label>Uang Muka</label>
                    <input type="text" name="uang_muka" class="form-control">
                  </div>
                  <div class="form-group">
                    <div class="float-right">
                      <button class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



    @endsection

    @section('js')

<script src="{{ asset('/') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Summernote -->
<script src="{{ asset('/') }}plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">
  
  $('.load_datatables').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  $('.load_editor').summernote();

  $(document).on('change','.itemBselect-1',function(){
    var id = $(this).val();
    var harga = $(this).find(':selected').data('harga');
    var stok = $(this).find(':selected').data('stok');
    $('.itemBharga-1').val(harga);
    $('.itemBhargasatuan-1').val(harga);
    $('.itemBjumlah-1').attr('max',stok);
    $('.load_stok-1').text(stok);
  });
  $(document).on('change','.itemBjumlah-1',function(){
    var jumlah = $(this).val();
    var harga = $('.itemBhargasatuan-1').val();
    var hasil = getTotal(harga,jumlah);
    $('.itemBharga-1').val(hasil);

  });
  $(document).on("keypress","[type='number']",function (evt) {
    evt.preventDefault();
});

  function getTotal(arg1,arg2) {
    return parseInt(arg1)*parseInt(arg2);
  }


  
</script>
    @endsection