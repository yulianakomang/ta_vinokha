@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
<style>
  /* Important part */
  .modal-dialog-custom{
    overflow-y: initial !important
  }
  .modal-body-custom{
    height: 450px;
    overflow-y: auto;
  }
  .break{
    padding: 10px;
    clear: both;
  }
</style>
@endsection

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Detail Sewa</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Detail Sewa</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<?php 

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}

function status($value='')
{
  $status = array(
    '<button class="btn btn-danger">Belum Diambil</button>',
    '<button class="btn btn-primary">Sudah Diambil</button>',
    '<button class="btn btn-success">Sudah Kembali</button>',
  );
  return $status[$value];
}

 ?>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">

      <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-6">
              <h2>#{{ $data['sewa']->kode }}</h2>
                  
                </div>
                <div class="col-md-6 text-right">
                  {!! status($data['sewa']->status_penyewaan) !!}
                </div>
              </div>
              <table class="table">
                <tr>
                  <td>Peminjam</td>
                  <td>:</td>
                  <td>{{ $data['sewa']->getUser->nama }}</td>
                </tr>
              </table>
            </div>
            <div class="col-md-6">
             <table class="table">
              <tr>
                <td>TGL Pinjam</td>
                <td>:</td>
                <td>{{ tgl_indo($data['sewa']->tgl_pinjam) }}</td>
              </tr>
              <tr>
                <td>TGL Kembali</td>
                <td>:</td>
                <td>{{ tgl_indo($data['sewa']->tgl_pengembalian) }}</td>
              </tr>
            </table>
          </div>
        </div>
        <div class="break"></div>
        <div class="row">
          <div class="col-md-6">
        <h3>Busana dipinjam</h3>
        </div>
        <div class="col-md-6 text-right">
          <h3>TOTAL {{ rupiah($data['sewa']->sub_total) }}</h3> 
        </div>
        </div>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama</th>
              <th scope="col">Ukuran</th>
              <th scope="col">Jumlah</th>
              <th scope="col">Harga / 3 hari</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
          @foreach($data['busana'] as $i =>$item)
            <tr>
              <th scope="row">{{ $i+1 }}</th>
              <td>{{ $item->getBusana->nama }}</td>
              <td>{{ $item->getBusana->size }}</td>
              <td>{{ $item->jumlah }}</td>
              <td>{{ rupiah($item->getBusana->harga) }}</td>
              <td>{{ rupiah($item->getBusana->harga*$item->jumlah) }}</td>
            </tr>
          </tbody>
          @endforeach
        </table>
        <div class="break"></div>
        <h3>Riwayat Transaksi</h3>

        
        <table class="table">
          <thead>
                <tr>
                  <th>#</th>
                  <th>TGL</th>
                  <th>Kode Sewa</th>
                  <th>Nominal</th>
                  <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                 @foreach($data['transaksi'] as $i =>$trans)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td>{{ tgl_indo(date('Y-m-d',strtotime($trans->created_at))) }}</td>
                  <td>{{ $trans->getSewa->kode }}</td>
                  <td>{{ rupiah($trans->nominal) }}</td>
                  <td>{{ $trans->keterangan }}</td>
                  
                </tr>
                @endforeach
                </tbody>
        </table>       

      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->



@endsection

@section('js')

<script src="{{ asset('/') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Summernote -->
<script src="{{ asset('/') }}plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">

  $('.load_datatables').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
  });
  $('.load_editor').summernote();

  $(document).on('change','.itemBselect-1',function(){
    var id = $(this).val();
    var harga = $(this).find(':selected').data('harga');
    var stok = $(this).find(':selected').data('stok');
    $('.itemBharga-1').val(harga);
    $('.itemBhargasatuan-1').val(harga);
    $('.itemBjumlah-1').attr('max',stok);
    $('.load_stok-1').text(stok);
  });
  $(document).on('change','.itemBjumlah-1',function(){
    var jumlah = $(this).val();
    var harga = $('.itemBhargasatuan-1').val();
    var hasil = getTotal(harga,jumlah);
    $('.itemBharga-1').val(hasil);

  });
  $(document).on("keypress","[type='number']",function (evt) {
    evt.preventDefault();
  });

  function getTotal(arg1,arg2) {
    return parseInt(arg1)*parseInt(arg2);
  }


  
</script>
@endsection