<!DOCTYPE html>
<html>
<head>
  <title>Cetak Penyewaan</title>
  <link rel="stylesheet" href="{{ asset('/') }}dist/css/adminlte.min.css">
</head>
<body>

<?php 

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}

function status($value='')
{
  $status = array(
    'Belum Diambil',
    'Sudah Diambil',
    'Sudah Kembali',
  );
  return $status[$value];
}

 ?>
<div class="container-fluid">
  <table class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>#</th>
      <th>Kode</th>
      <th>Peminjam</th>
      <th>TGL Pinjam</th>
      <th>TGL Kembali</th>
      <th>Total</th>
      <th>Status</th>
    </tr>
    </thead>
    <tbody>
      @foreach($result['lists'] as $i => $item)
    <tr>
      <td>{{ $i+1 }}</td>
      <td>{{ $item->kode }}</td>
      <td>{{ $item->getUser->nama }}</td>
      <td>{{ tgl_indo($item->tgl_pinjam) }}</td>
      <td>{{ tgl_indo($item->tgl_pengembalian) }}</td>
      <td>{{ rupiah($item->sub_total) }}</td>
      <td>{{ status($item->status_penyewaan) }}</td>
      
    </tr>
    @endforeach
    </tbody>
   
  </table>
  
</div>

<script type="text/javascript">
  window.print();
  window.onfocus=function(){ window.close();}
</script>

</body>
</html>