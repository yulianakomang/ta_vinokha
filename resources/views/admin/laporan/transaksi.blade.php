@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
  <style>
    /* Important part */
.modal-dialog-custom{
    overflow-y: initial !important
}
.modal-body-custom{
    height: 450px;
    overflow-y: auto;
}
  </style>
@endsection

@section('content')

<?php 

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}

function status($value='')
{
  $status = array(
    'Belum Diambil',
    'Sudah Diambil',
    'Sudah Kembali',
  );
  return $status[$value];
}

 ?>

 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Laporan Transaksi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Laporan Transaksi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <form action="">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Dari</label>
                    <input type="date" name="dari" class="form-control" value="{{ @$_GET['dari'] }}">
                  </div>
                  
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Sampai</label>
                    <input type="date" name="sampai" class="form-control" value="{{ @$_GET['sampai'] }}">
                  </div>
                  
                </div>
                <div class="col-md-3 offset-md-3">
                  <label>Opsi</label> <br>
                  <button class="btn btn-primary"><i class="fa fa-search"></i> Lihat Data</button>
                  <button class="btn btn-success btn-cetak"><i class="fa fa-print"></i> Cetak Data</button>
                </div>
              </div>
              </form>

              <table class="table table-bordered table-striped load_datatables">
                <thead>
                <tr>
                  <th>#</th>
                  <th>TGL</th>
                  <th>Kode Sewa</th>
                  <th>Nominal</th>
                  <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($result['lists'] as $i => $item)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td>{{ tgl_indo(date('Y-m-d',strtotime($item->created_at))) }}</td>
                  <td>{{ @$item->getSewa->kode }}</td>
                  <td>{{ rupiah($item->nominal) }}</td>
                  <td>{{ $item->keterangan }}</td>
                  
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



    @endsection

    @section('js')

<script src="{{ asset('/') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Summernote -->
<script src="{{ asset('/') }}plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">
  $('.btn-cetak').click(function(ev){
    ev.preventDefault();
    var crrurl = window.location.href;
    <?php if (@$_GET['dari']) { ?>
    window.open(crrurl+'&cetak=1','_target');
    <?php }else{ ?>
    window.open(crrurl+'?cetak=1','_target');
    <?php } ?>
   });
  
  $('.load_datatables').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  $('.load_editor').summernote();

  
</script>
    @endsection