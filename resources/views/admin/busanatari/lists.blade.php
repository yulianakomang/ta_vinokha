@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
  <style>
    /* Important part */
.modal-dialog-custom{
    overflow-y: initial !important
}
.modal-body-custom{
    height: 450px;
    overflow-y: auto;
}
  </style>
@endsection

@section('content')

 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Busana Tari</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Busana Tari</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <button class="btn btn-success mb-4" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i>  Tambah</button>

              <table class="table table-bordered table-striped load_datatables">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Kode</th>
                  <th>Nama</th>
                  <th>Kategori</th>
                  <th>Harga</th>
                  <th>Stok</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($result['lists'] as $i => $item)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td><a href="{{ route('fotobusana-lists',[$item->id]) }}" class="btn btn-primary">{{ $item->kode }}</a></td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $item->getCategory->nama }}</td>
                  <td>{{ $fx->Rp($item->harga) }}</td>
                  <td>{{ $item->stok }}</td>
                  <td>
                    <button class="btn btn-sm btn-primary btn_edit_data" data-id="{{ $item->id }}" data-url="{{ route('busanatari-edit',['id'=>$item->id]) }}" data-toggle="modal" data-target="#EditModal"><i class="fa fa-edit"></i>  Edit</button>
                    <a href="{{ route('busanatari-delete',['id'=> $item->id]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-trash"></i>  Delete</a>
                  </td>
                  
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Modal -->
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-custom" role="document">
      <div class="modal-content">
        <form action="{{ route('busanatari-store') }}" id="form-on-click" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modal-body-custom">
          <div class="form-group">
            <label>Kode</label>
            <input type="text" name="kode" class="form-control" readonly value="BT-<?= rand(11111,99999) ?>">
          </div>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control">
          </div>
          <div class="form-group">
            <label>Kategori</label>
            <select name="id_kategori" id="" class="form-control">
              @foreach($result['cats'] as $item)
              <option value="{{ $item->id }}">{{ $item->nama }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>Size</label>
            <input type="text" name="size" class="form-control">
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" class="form-control" cols="30" rows="10"></textarea>
          </div>
          <div class="form-group">
            <label>Harga</label>
            <input type="number" name="harga" class="form-control">
          </div>
          <div class="form-group">
            <label>Stok</label>
            <input type="number" name="stok" class="form-control">
          </div>
          <div class="form-group">
            <label>Foto</label>
            <input type="file" name="file" class="form-control">
          </div>
          <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
              <option value="1">Tersedia</option>
              <option value="0">Habis</option>
            </select>
          </div>

         
        </div>
        <div class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>  Close</a>
          <button type="submit" class="btn btn-primary btn-on-click"><i class="fa fa-save"></i>  Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-custom" role="document">
      <div class="modal-content">
        <form action="{{ route('busanatari-update') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modal-body-custom">
          <input type="hidden" name="id" class="form-id">
          <div class="form-group">
            <label>Kode</label>
            <input type="text" name="kode" class="form-control form-kode" readonly>
          </div>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control form-nama">
          </div>
          <div class="form-group">
            <label>Kategori</label>
            <select name="id_kategori" id="" class="form-control form-id_kategori">
              @foreach($result['cats'] as $item)
              <option value="{{ $item->id }}">{{ $item->nama }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>Size</label>
            <input type="text" name="size" class="form-control form-size">
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" class="form-control form-keterangan" cols="30" rows="10"></textarea>
          </div>
          <div class="form-group">
            <label>Harga</label>
            <input type="number" name="harga" class="form-control form-harga">
          </div>
          <div class="form-group">
            <label>Stok</label>
            <input type="number" name="stok" class="form-control form-stok">
          </div>
          <div class="form-group">
            <label>Foto</label>
            <input type="file" name="file" class="form-control">
          </div>
          <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control form-status">
              <option value="1">Tersedia</option>
              <option value="0">Habis</option>
            </select>
          </div>
          
        </div>
        <div class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>  Close</a>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>  Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>



    @endsection

    @section('js')

<script src="{{ asset('/') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Summernote -->
<script src="{{ asset('/') }}plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">
  
  $('.load_datatables').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  $('.load_editor').summernote();

  $('.btn-on-click').click(function(){
    $(this).attr('disabled','disabled');
    $('#form-on-click').submit();
  });

  $(document).on('click','.btn_edit_data',function(){
    var id = $(this).data('id');
    var url = $(this).data('url');
    $.get(url,function(res){
      var result = $.parseJSON(JSON.stringify(res));
      $.each(result, function(i,val){
        if(i === 'content'){
          $('#EditModal').find('.note-editable').html(val);
        }else{
          $('.form-'+i).val(val);
        }
      });
    });
  });

</script>
    @endsection