@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
  <style>
    /* Important part */
.modal-dialog-custom{
    overflow-y: initial !important
}
.modal-body-custom{
    height: 450px;
    overflow-y: auto;
}
  </style>
@endsection

@section('content')

 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Album dari {{ $data['busana']->nama }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Album dari {{ $data['busana']->nama }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <form action="{{ route('fotobusana-store') }}" id="form-on-click" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id_busana" value="{{ $data['busana']->id }}">
              <div class="form-group">
                <label>Pilih Foto (Bisa lebih dari 1)</label>
                <input type="file" name="file[]" class="form-control" multiple>
                <small>*Upload foto baru akan menggantikan yg lama.</small>
              </div>
              <div class="form-group">
                <button class="btn btn-success"><i class="fa fa-arrow-up"></i>  Unggah</button>
              </div>
            </form>

              <h3>Foto Album Saat ini</h3>
              <div class="row">
                @forelse($data['foto'] as $i)
                    <div class="col-md-3">
                  <img src="{{ asset('uploads/'.$i->foto ) }}" class="w-100">
                  <hr>
                  <a href="{{ route('fotobusana-delete',[$i->id]) }}" class="btn btn-danger w-100"><i class="fa fa-trash"></i> Hapus</a>
                </div>
                @empty
                    <p>Belum ada Album.</p>
                @endforelse
              
                
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    @endsection
