@extends('admin.parts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('/') }}plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- summernote -->
  <link rel="stylesheet" href="{{ asset('/') }}plugins/summernote/summernote-bs4.css">
  <style>
    /* Important part */
.modal-dialog-custom{
    overflow-y: initial !important
}
.modal-body-custom{
    height: 450px;
    overflow-y: auto;
}
  </style>
@endsection

@section('content')

 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Users Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users Management</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <button class="btn btn-success mb-4" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i>  Add</button>

              <table class="table table-bordered table-striped load_datatables">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                  <?php $level = array(1=>'Owner','Admin','Member') ?>
                  @foreach($result['lists'] as $i => $item)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $level[$item->level] }}</td>
                  <td>
                    <button class="btn btn-sm btn-primary btn_edit_data" data-id="{{ $item->id }}" data-url="{{ route('users-edit',['id'=>$item->id]) }}" data-toggle="modal" data-target="#EditModal"><i class="fa fa-edit"></i>  Edit</button>
                    <a href="{{ route('users-delete',['id'=> $item->id]) }}" class="btn btn-sm btn-secondary"><i class="fa fa-trash"></i>  Delete</a>
                  </td>
                  
                </tr>
                @endforeach
                </tbody>
               
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Modal -->
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-custom" role="document">
      <div class="modal-content">
        <form action="{{ route('users-store') }}" id="form-on-click" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modal-body-custom">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control">
          </div>
          <div class="form-group">
            <label>No Telp</label>
            <input type="text" name="no_telp" class="form-control">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <input type="text" name="alamat" class="form-control">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control">
          </div>
          <div class="form-group">
            <label>No KTP</label>
            <input type="text" name="no_ktp" class="form-control">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" class="form-control">
          </div>

          <div class="form-group">
            <label>Level</label>
            <select name="level" id="" class="form-control">
              <option value="1">Owner</option>
              <option value="2">Admin</option>
              <option value="3">Member</option>
            </select>
          </div>


        </div>
        <div class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>  Close</a>
          <button type="submit" class="btn btn-primary btn-on-click"><i class="fa fa-save"></i>  Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-custom" role="document">
      <div class="modal-content">
        <form action="{{ route('users-update') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modal-body-custom">
          <input type="hidden" name="id" class="form-id">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control form-nama">
          </div>
          <div class="form-group">
            <label>No Telp</label>
            <input type="text" name="no_telp" class="form-control form-no_telp">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <input type="text" name="alamat" class="form-control form-alamat">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control form-email">
          </div>
          <div class="form-group">
            <label>No KTP</label>
            <input type="text" name="no_ktp" class="form-control form-no_ktp">
          </div>
          <div class="form-group">
            <label>Password (isi untuk mengubah)</label>
            <input type="text" name="password" class="form-control">
          </div>

          <div class="form-group">
            <label>Level</label>
            <select name="level" id="" class="form-control form-level">
              <option value="1">Owner</option>
              <option value="2">Admin</option>
              <option value="3">Member</option>
            </select>
          </div>
          
        </div>
        <div class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>  Close</a>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>  Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>



    @endsection

    @section('js')

<script src="{{ asset('/') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Summernote -->
<script src="{{ asset('/') }}plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">
  
  $('.load_datatables').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  $('.load_editor').summernote();

  $('.btn-on-click').click(function(){
    $(this).attr('disabled','disabled');
    $('#form-on-click').submit();
  });

  $(document).on('click','.btn_edit_data',function(){
    var id = $(this).data('id');
    var url = $(this).data('url');
    $.get(url,function(res){
      var result = $.parseJSON(JSON.stringify(res));
      $.each(result, function(i,val){
        if(i === 'content'){
          $('#EditModal').find('.note-editable').html(val);
        }else{
          $('.form-'+i).val(val);
        }
      });
    });
  });

</script>
    @endsection