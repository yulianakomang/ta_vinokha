<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/','FrontController@Home')->name('home');

/* Login System */
Route::get('login', function () {
	return view('auth.login');
});
Route::get('register', function () {
	return view('front.register');
});
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
// Route::get('/register', 'Front\IndexController@register')->name('register');
Route::post('/register/store', 'Front\IndexController@addUser')->name('register-store');


/* Role Login */
Route::group([
	'middleware' => ['auth'],
	'prefix' => 'admin'
], function(){
	
	/* Dashboard */
	Route::get('/', 'DashController@index')->name('dashboard');

	/* Daftar Sewa*/
	Route::get('sewa-lists', 'SewaController@index')->name('sewa-lists');
	Route::get('sewa-add', 'SewaController@add')->name('sewa-add');
	Route::get('sewa-edit/{id}', 'SewaController@edit')->name('sewa-edit');
	Route::post('sewa-store', 'SewaController@store')->name('sewa-store');
	Route::post('sewa-update', 'SewaController@update')->name('sewa-update');
	Route::get('sewa-delete/{id}', 'SewaController@destroy')->name('sewa-delete');
	Route::get('sewa-detail/{id}', 'SewaController@detail_sewa')->name('sewa-detail');
	// Ajax Request
	Route::get('sewa-get/{id}', 'SewaController@get')->name('sewa-get');
	Route::get('sewa-get-kembali/{id}', 'SewaController@get_kembali')->name('sewa-get-kembali');
	// Status Penyewaan
	Route::get('sewa-set1/{id}', 'SewaController@set1')->name('sewa-set1');
	Route::get('sewa-set2/{id}', 'SewaController@set2')->name('sewa-set2');
	Route::get('sewa-set3/{id}', 'SewaController@set3')->name('sewa-set3');
	// Online Booking
	Route::get('sewa-online-lists', 'SewaController@online_lists')->name('sewa-online-lists');
	Route::get('sewa-online-terima/{id}', 'SewaController@online_terima')->name('sewa-online-terima');

	/* Kategori*/
	Route::get('kategori-lists', 'CategoryController@index')->name('kategori-lists');
	Route::get('kategori-edit/{id}', 'CategoryController@edit')->name('kategori-edit');
	Route::post('kategori-store', 'CategoryController@store')->name('kategori-store');
	Route::post('kategori-update', 'CategoryController@update')->name('kategori-update');
	Route::get('kategori-delete/{id}', 'CategoryController@destroy')->name('kategori-delete');

	/* Busanatari*/
	Route::get('busanatari-lists', 'BusanatariController@index')->name('busanatari-lists');
	Route::get('busanatari-edit/{id}', 'BusanatariController@edit')->name('busanatari-edit');
	Route::post('busanatari-store', 'BusanatariController@store')->name('busanatari-store');
	Route::post('busanatari-update', 'BusanatariController@update')->name('busanatari-update');
	Route::get('busanatari-delete/{id}', 'BusanatariController@destroy')->name('busanatari-delete');
	// Foto Busana
	Route::get('fotobusana-lists/{id_busana}', 'BusanatariController@fotobusana')->name('fotobusana-lists');
	Route::post('fotobusana-store', 'BusanatariController@fotobusana_store')->name('fotobusana-store');
	Route::get('fotobusana-delete/{id}', 'BusanatariController@fotobusana_destroy')->name('fotobusana-delete');

	/* Users*/
	Route::get('users-lists', 'UserController@index')->name('users-lists');
	Route::get('users-edit/{id}', 'UserController@edit')->name('users-edit');
	Route::post('users-store', 'UserController@store')->name('users-store');
	Route::post('users-update', 'UserController@update')->name('users-update');
	Route::get('users-delete/{id}', 'UserController@destroy')->name('users-delete');

	/* Laporan */
	Route::get('laporan-penyewaan', 'LaporanController@penyewaan')->name('laporan-penyewaan');
	Route::get('laporan-transaksi', 'LaporanController@transaksi')->name('laporan-transaksi');


});

Route::middleware('auth')->group(function(){

	
});

// for public
Route::get('/', 'Front\IndexController@index')->name('public-index');

Route::get('/contact', 'Front\IndexController@contact')->name('public-contact');
Route::get('/about', 'Front\IndexController@about')->name('public-about');
Route::get('/list', 'Front\IndexController@list')->name('public-list');
Route::get('/detail/{id}', 'Front\IndexController@detailproduct')->name('public-detailproduct');
Route::get('/cart', 'Front\CartController@cart')->name('public-cart');
Route::post('/cart/{id}', 'Front\CartController@addToCart')->name('public-addToCart');
Route::get('/cart/delete/{id}', 'Front\CartController@deleteCart')->name('public-deleteCart');
Route::get('/checkout', 'Front\IndexController@checkout')->name('public-checkout');
Route::get('/payment', 'Front\PaymentController@payment')->name('public-payment');
Route::get('/public-login', 'Front\IndexController@login')->name('public-login');
Route::get('/public-register', 'Front\IndexController@register')->name('public-register');
Route::post('/confirm', 'Front\PaymentController@confirm')->name('public-confirm');