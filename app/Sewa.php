<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sewa extends Model
{
    protected $table ='tb_sewa';
    protected $guarded = [];

    public function getUser()
    {
    	return $this->belongsTo('App\User','id_user');
    }
}
