<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotoBusana extends Model
{
    protected $table ='tb_fotobusana';
    protected $guarded = [];

    public function getBusana()
    {
    	return $this->belongsTo('App\Busanatari','id_busana');
    }
}
