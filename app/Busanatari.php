<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Busanatari extends Model
{
    protected $table ='tb_busanatari';
    protected $guarded = [];

    public function getCategory()
    {
    	return $this->belongsTo('App\Category','id_kategori');
    }
}
