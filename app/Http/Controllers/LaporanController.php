<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sewa;
use App\Transaksi;

class LaporanController extends Controller
{
    public function penyewaan(Request $request)
    {
        if ($request->dari <> '' && $request->sampai <> '') {
            $result['lists'] = Sewa::where('setujui',1)
            ->where('status_penyewaan',$request->status)
            ->where('tgl_pinjam','>=',$request->dari)
            ->where('tgl_pinjam','<=',$request->sampai)
            // ->orWhere('tgl_pengembalian','>=',$request->dari)
            // ->where('tgl_pengembalian','<=',$request->sampai)
            ->get();
        }else{            
            $result['lists'] = Sewa::where('setujui',1)->get();
        }

        if (@$request->cetak == 1) {
            return view('admin.laporan.cetak-penyewaan', compact('result'));    
        }else{
            return view('admin.laporan.penyewaan', compact('result'));    
        }

    }

    public function transaksi(Request $request)
    {
        if ($request->dari <> '' && $request->sampai <> '') {
            $result['lists'] = Transaksi::where('created_at','>=',$request->dari)->where('created_at','<=',$request->sampai)
            ->get();
        }else{            
            $result['lists'] = Transaksi::get();
        }
        if (@$request->cetak == 1) {
            return view('admin.laporan.cetak-transaksi', compact('result'));
        }else{  
            return view('admin.laporan.transaksi', compact('result'));
        }
     }
}
