<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sewa;
use App\User;
use App\Category;
use App\Busanatari;
use App\DetailSewa;
use App\Transaksi;

class DashController extends Controller
{
    public function index($value='')
    {
    	if (Auth::user()->level == 3) {
    		return redirect(route('public-cart'));
    	}
    	$data['busana'] = Busanatari::count();
    	$data['sewa'] = Sewa::count();
    	$data['kategori'] = Category::count();
    	$data['member'] = User::where('level',3)->count();
    	return view('admin.dashboard',compact('data'));
    }
}
