<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Payment;
use App\Busanatari;
use App\Sewa;
use App\DetailSewa;
use App\Transaksi;
use App\User;
use Session;
use Auth;
use App\Mail\SendEmail;


class PaymentController extends Controller
{
    public $email_admin = "rahtiaanandapremavinokha@gmail.com";

    public function payment(Request $request)
    {
    	$cart = Session::get('cart');
    	$pinjam= request('tgl_pinjam');
    	$kembali= Date('y:m:d', strtotime("+4 days"));
    	$random = rand(11111,99999);
    	$kode='SW-'.$random;
    	$id_user= Auth::id();
    	if ($id_user==null) {
    		 return Redirect()->route('public-cart')->withErrors(['Silahkan Login terlebih dahulu', 'The Message']);
    	}
		$total=request('total');
    	$status='1';
    	$online='1';

    	$data=[
            'id_user' => $id_user,
            'kode' => $kode,
            'tgl_pinjam' => $pinjam,
            'tgl_pengembalian' => $kembali,
            'sub_total' => $total,
            'status_penyewaan' => $status,
            'online' => $online,
        ];
        return view('front.confirm', compact('data'));

    }

    public function confirm(Request $request)
    {
    	$data=[
            'id_user' => request('id_user'),
            'kode' => request('kode'),
            'tgl_pinjam' => request('tgl_pinjam'),
            'tgl_pengembalian' => date('Y-m-d',strtotime(request('tgl_pinjam'). '+3 days')),
            'sub_total' => request('sub_total'),
            'online' => 1,
        ];
        if ($request->hasFile('image')) {
            $fileName = time().'.'.$request->image->getClientOriginalExtension();  
            $request->image->move(public_path('uploads'), $fileName);
            $data['bukti_tf'] = $fileName;
        }
        $id_sewa = Sewa::create($data);
        // Busana
        $cart = Session::get('cart');
        foreach ($cart as $key => $item) {
            $data_busana = array(
                'id_sewa' => $id_sewa->id,
                'id_busana' => $item['id'],
                'jumlah' => $item['jumlah'],
            );
            DetailSewa::create($data_busana);

        }
        // record transaksi
        $data_transaksi = array(
            'id_sewa' => $id_sewa->id,
            'nominal' => request('sub_total')/2,
            'keterangan' => 'Uang Muka (DP)',
        );
        Transaksi::create($data_transaksi);

        $data['sewa'] = $id_sewa;
        $data['busana'] = DetailSewa::where('id_sewa',$id_sewa->id)->get();
        $data['transaksi'] = Transaksi::where('id_sewa',$id_sewa->id)->get();
         // Kirim Email
        $user = User::find($id_sewa->id_user);
        $kirimemail = array($user->email,$this->email_admin);
        // Kirim ke Admin
        \Mail::to($kirimemail)->send(new SendEmail($data,'emails.book_online','Booking Online Busana'));
        $request->session()->forget('cart');
        return Redirect()->route('public-index')->withErrors(['Terimakasih telah menyewa ', 'The Message']);
    }
}
