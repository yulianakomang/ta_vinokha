<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Busanatari;
use App\Category;
use Session;

class CartController extends Controller
{
	public function cart()
	{
		
		return view('front.cart');
	}
	public function addToCart(Request $request, $id)
	{
		$product = Busanatari::where('id',$id)->get();
		// dd($product);
		$cart = Session::get('cart');
		$jumlah = request('jumlah');
		$harga=$product[0]->harga;
		$total=$harga*$jumlah;
		$cart[$product[0]->id] = array(
			"id" => $product[0]->id,
			"id_kategori" => $product[0]->id_kategori,
			"kode" => $product[0]->kode,
			"nama" => $product[0]->nama,
			"size" => $product[0]->size,
			"keterangan" => $product[0]->keterangan,
			"jumlah" => $jumlah,
			"harga" => $product[0]->harga,
			"total" => $total,
			"foto" => $product[0]->foto,
			"stok" => $product[0]->stok,
		);
		// dd($cart[$product[0]->id]);
		Session::put('cart', $cart);
		Session::flash('success','barang berhasil ditambah ke keranjang!');
		return redirect()->route('public-cart');
	}

	public function updateCart(Request $cartdata)
	{
		$cart = Session::get('cart');
		foreach ($cartdata->all() as $id => $val) 
		{
			if ($val > 0) {
				$cart[$id]['qty'] += $val;
			} else {
				unset($cart[$id]);
			}
		}
		Session::put('cart', $cart);
		return redirect()->route('public-cart');
	}

	public function deleteCart($id)
	{
		$cart = Session::get('cart');
		unset($cart[$id]);
		Session::put('cart', $cart);
		return redirect()->route('public-cart');
	}
}
