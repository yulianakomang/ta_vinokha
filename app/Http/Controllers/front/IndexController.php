<?php

namespace App\ Http\ Controllers\ front;

use App\ Http\ Controllers\ Controller;
use Illuminate\ Http\ Request;
use App\ Busanatari;
use App\ FotoBusana;
use App\ Category;
use App\ User;
use Session;
use Auth;
use Illuminate\ Support\ Facades\ Hash;

class IndexController extends Controller {

    public function index() {

        $data = Busanatari::all();
        return view('front.homepage', compact('data'));
    }
    public function list(Request $request) {

        if (!empty($request -> kategori)) {
            $category = Category::where('nama', $request -> kategori) -> first();
            $id = $category -> id;
            // dd($id);
            $data = Busanatari::where('id_kategori', $id) -> paginate(15);
            $jml_data = Busanatari::where('id_kategori', $id) -> count();
        } else {
            $data = Busanatari::paginate(15);
            $jml_data = Busanatari::all() -> count();
        }
        return view('front.list', compact('data', 'jml_data'));
    }
    public function detailproduct($id) {
        $data = Busanatari::where('id', $id)->get();
        $album = FotoBusana::where('id_busana', $id)->get();
        return view('front.singleproduct', compact('data','album'));
    }
    public function checkout() {
        return view('front.cart');
    }
    public function about() {
        return view('front.about');
    }
    public function contact() {
        return view('front.contact');
    }
    public function addUser(Request $request) {
        // dd($request->all());
        $data = [
            'nama' => request('nama'),
            'no_telp' => request('no_telp'),
            'no_ktp' => request('no_ktp'),
            'alamat' => request('alamat'),
            'level' => request('level'),
            'user_image' => request('user_image'),
            'email' => request('email'),
            'password' => request('password'),
        ];
        User::create([
            'nama' => $data['nama'],
            'no_telp' => $data['no_telp'],
            'no_ktp' => $data['no_ktp'],
            'alamat' => $data['alamat'],
            'level' => $data['level'],
            'user_image' => $data['user_image'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect() -> route('public-login');
    }
    public function login()
    {
        return view('front.cart');
    }
    public function register()
    {
        return view('front.cart');
    }
}