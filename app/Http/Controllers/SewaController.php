<?php

namespace App\Http\Controllers;

use App\Sewa;
use App\User;
use App\Busanatari;
use App\DetailSewa;
use App\Transaksi;
use App\Mail\SendEmail;
use Illuminate\Http\Request;

class SewaController extends Controller
{

    public $email_admin = "rahtiaanandapremavinokha@gmail.com";

    public function index()
    {
        $result['lists'] = Sewa::where('setujui',1)->orderBy('id','DESC')->get();
        return view('admin.sewa.lists', compact('result'));
    }

    public function detail_sewa($id='')
    {
        $data['sewa'] = Sewa::find($id);
        $data['busana'] = DetailSewa::where('id_sewa',$id)->get();
        $data['transaksi'] = Transaksi::where('id_sewa',$id)->get();

        return view('admin.sewa.detailsewa',compact('data'));
    }

    public function online_lists()
    {
        $result['lists'] = Sewa::where('setujui',0)->orderBy('id','DESC')->get();
        return view('admin.sewa.online-lists', compact('result'));    
    }

    public function online_terima($id="")
    {
        $db_sewa = Sewa::find($id);
        $data_sewa = array(
            'setujui'=> 1,
        );
        $db_sewa->update($data_sewa);

        $data['nama'] = $db_sewa->getUser->nama;
        $data['msg'] = 'Selamat! Pesanan anda sudah kami terima dan kami sudah proses. Silahkan ambil busana-nya sesuai tanggal booking ya.';

         // Kirim Email
        \Mail::to($this->email_admin)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));
        // \Mail::to($db_sewa->getUser->email)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));

        return redirect()->back()->with('status','success');
    }

    public function add()
    {
        $result['users'] = User::where('level',3)->get();
        $result['busana'] = Busanatari::where('stok','>',0)->get();
        // dd($result);
        
        return view('admin.sewa.form', compact('result'));
    }

    public function store(Request $request)
    {
        $data_sewa = array(
            'kode'=> $request->kode,
            'id_user'=> $request->id_user,
            'tgl_pinjam'=> $request->tgl_pinjam,
            'tgl_pengembalian'=> $request->tgl_pengembalian,
            'sub_total'=> $request->sub_total,
            'setujui'=> 1,
        );
        $id_sewa = Sewa::create($data_sewa);

        for ($i=0; $i < count($request->id_busana); $i++) { 
            $data_busana = array(
                'id_sewa' => $id_sewa->id,
                'id_busana' => $request->id_busana[$i],
                'jumlah' => $request->jumlah[$i],
            );
            DetailSewa::create($data_busana);

        }

        $data_transaksi = array(
            'id_sewa' => $id_sewa->id,
            'nominal' => $request->uang_muka,
            'keterangan' => 'Uang Muka (DP)',
        );
        Transaksi::create($data_transaksi);


        return redirect(route('sewa-lists'))->with('status','success');
    }

    public function edit($id)
    {
        $data['sewa'] = Sewa::find($id);
        $data['busana'] = DetailSewa::where('id_sewa',$id)->get();
        $data['transaksi'] = Transaksi::where(['id_sewa'=>$id,'status'=>0])->first();
        // dd($data['transaksi']);
        $result['users'] = User::where('level',3)->get();
        $result['busana'] = Busanatari::where('stok','>',0)->get();

        return view('admin.sewa.form-edit', compact('data','result'));
    }

    public function update(Request $request)
    {
        $db_sewa = Sewa::find($request->id);
        $data_sewa = array(
            'kode'=> $request->kode,
            'id_user'=> $request->id_user,
            'tgl_pinjam'=> $request->tgl_pinjam,
            'tgl_pengembalian'=> $request->tgl_pengembalian,
            'sub_total'=> $request->sub_total,
        );
        $db_sewa->update($data_sewa);

        // hapus data lama
        DetailSewa::where('id_sewa',$request->id)->delete();
        // update data baru
        for ($i=0; $i < count($request->id_busana); $i++) { 
            $data_busana = array(
                'id_sewa' => $request->id,
                'id_busana' => $request->id_busana[$i],
                'jumlah' => $request->jumlah[$i],
            );
            DetailSewa::create($data_busana);

        }

        Transaksi::where('id_sewa',$request->id)->delete();
        $data_transaksi = array(
            'id_sewa' => $request->id,
            'nominal' => $request->uang_muka,
            'keterangan' => 'Uang Muka (DP)',
        );
        Transaksi::create($data_transaksi);

        return redirect()->back()->with('status','success');
    }

    public function destroy($id)
    {
        $result = Sewa::find($id)->delete();
        DetailSewa::where('id_sewa',$id)->delete();
        Transaksi::where('id_sewa',$id)->delete();
        return redirect()->back()->with('status','success');
    }

    public function set1($id='')
    {
        $db_sewa = Sewa::find($id);
        $data_sewa = array(
            'status_penyewaan'=> 0,
        );
        $db_sewa->update($data_sewa);

        $data['nama'] = $db_sewa->getUser->nama;
        $data['msg'] = 'Pesanan anda belum diambil.';

        // Kirim Email
        \Mail::to($this->email_admin)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));
        // \Mail::to($db_sewa->getUser->email)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));

        return redirect()->back()->with('status','success');
    }
    public function set2($id='')
    {
        $db_sewa = Sewa::find($id);
        $data_sewa = array(
            'status_penyewaan'=> 1,
        );
        $db_sewa->update($data_sewa);

        // catat data transaksi
        $trans = Transaksi::where(['id_sewa'=>$id,'status'=>0])->first();
        $data_transaksi = array(
            'id_sewa' => $id,
            'nominal' => $db_sewa->sub_total-$trans->nominal,
            'keterangan' => 'Pelunasan (Pengambilan)',
        );
        Transaksi::create($data_transaksi);

        // Pengurangan Stock terhadap Busana Tari
        $data_busana = DetailSewa::where('id_sewa',$id)->get();
        foreach ($data_busana as $item) {
            $busana = Busanatari::find($item->id_busana)->decrement('stok',$item->jumlah);
        }

        $data['nama'] = $db_sewa->getUser->nama;
        $data['msg'] = 'Pesanan anda sudah diambil.';

        // Kirim Email
        \Mail::to($this->email_admin)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));
        // \Mail::to($db_sewa->getUser->email)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));

        return redirect()->back()->with('status','success');
    }
    public function set3($id='')
    {
        $db_sewa = Sewa::find($id);
        $data_sewa = array(
            'status_penyewaan'=> 2,
        );
        $db_sewa->update($data_sewa);
        // Pengembalian Stock terhadap Busana Tari
        $data_busana = DetailSewa::where('id_sewa',$id)->get();
        foreach ($data_busana as $item) {
            $busana = Busanatari::find($item->id_busana)->increment('stok',$item->jumlah);
        }

        // Masukan Denda
        $tgl_sekarang = date('Y-m-d');
        $tgl_seharusnya = date('Y-m-d',strtotime($db_sewa->tgl_pengembalian));
        $start_date = new \DateTime($tgl_sekarang);
        $end_date = new \DateTime($tgl_seharusnya);
        $interval = $start_date->diff($end_date);
        if ($start_date > $end_date) {
            $data_transaksi = array(
                'id_sewa' => $id,
                'nominal' => ($db_sewa->sub_total/2)*$interval->days,
                'keterangan' => 'Denda! karena lewat '.$interval->days.' hari dari seharusnya.',
            );
            Transaksi::create($data_transaksi);
        }

         $data['nama'] = $db_sewa->getUser->nama;
        $data['msg'] = 'Barang sudah kami terima kembali, terimakasih sudah mempercayakan kami untuk penyewaan busana.';

        // Kirim Email
        \Mail::to($this->email_admin)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));
        // \Mail::to($db_sewa->getUser->email)->send(new SendEmail($data,'emails.status_pesanan','Status Pesanan'));

        return redirect()->back()->with('status','success');
    }

    function rupiah($angka){
  
          $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
          return $hasil_rupiah;
         
        }

    public function get($id='')
    {
        $data = Sewa::find($id);
        $trans = Transaksi::where(['id_sewa'=>$id,'status'=>0])->first();
        $result = array(
            'kode' => $data->kode,
            'total' => $this->rupiah($data->sub_total),
            'dp' =>$this->rupiah($trans->nominal),
            'sisa' =>$this->rupiah($data->sub_total-$trans->nominal),
        );

        return response()->json($result);
    }

    public function get_kembali($id='')
    {
       $data = Sewa::find($id);
        $trans = Transaksi::where(['id_sewa'=>$id,'status'=>0])->first();
        $result = array(
            'kode' => $data->kode,
            'total' => $this->rupiah($data->sub_total),
            'kembali_tempo' =>$data->tgl_pengembalian,
            'tgl_sewa' =>$data->tgl_pinjam,
        );

        return response()->json($result);    }


}
