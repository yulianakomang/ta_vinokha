<?php

namespace App\Http\Controllers;

use App\Destinations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class DestinationsController extends Controller
{
    
    public function index()
    {
        $result['lists'] = Destinations::get();
        return view('admin.destinations.lists', compact('result'));
    }

    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $filename = time().'-'.$request->file('image')->getClientOriginalName();
            Storage::putFileAs('/', $request->image, $filename,'public');
            Destinations::create($request->except('image') + ['photo'=>$filename]);
        }else{
            Destinations::create($request->except('image'));
        }
        return redirect()->back();
    }

    public function edit($id)
    {
        $result = Destinations::find($id);

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $result = Destinations::find($request->id);
        if ($request->hasFile('image')) {
            Storage::delete($result->photo);
            $filename = time().'-'.$request->file('image')->getClientOriginalName();
            Storage::putFileAs('/', $request->image, $filename,'public');
            $result->update($request->except('image') + ['photo'=>$filename]);
        }else{
            $result->update($request->except('image'));
        }

        return redirect()->back()->with('msg','success');
    }

    public function destroy($id)
    {
        $result = Destinations::findOrFail($id);
        Storage::delete($result->photo);
        $result->delete();
        return redirect()->back()->with('msg','success');
    }
}
