<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $result['lists'] = User::get();
        return view('admin.users.lists', compact('result'));
    }

    public function store(Request $request)
    {
        $pwd = Hash::make($request->password);
        User::create($request->except('password') + ['password'=>$pwd]);
        return redirect()->back()->with('status','success');
    }

    public function edit($id)
    {
        $result = User::find($id);

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $result = User::find($request->id);
        if ($request->password <> '') {
            $pwd = Hash::make($request->password);
            $result->update($request->except(['_token','password']) + ['password'=>$pwd]);
        }else{
            $result->update($request->except(['_token','password']));
        }

        return redirect()->back()->with('status','success');
    }

    public function destroy($id)
    {
        $result = User::find($id)->delete();
        return redirect()->back()->with('status','success');
    }
}
