<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $result['lists'] = Category::get();
        return view('admin.kategori.lists', compact('result'));
    }

    public function store(Request $request)
    {
        Category::create($request->all());
        return redirect()->back()->with('status','success');
    }

    public function edit($id)
    {
        $result = Category::find($id);

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $result = Category::find($request->id);
        $result->update($request->except('_token'));

        return redirect()->back()->with('status','success');
    }

    public function destroy($id)
    {
        $result = Category::find($id)->delete();
        return redirect()->back()->with('status','success');
    }
}
