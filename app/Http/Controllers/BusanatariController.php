<?php

namespace App\Http\Controllers;

use App\Busanatari;
use App\FotoBusana;
use App\Category;
use Illuminate\Http\Request;

class BusanatariController extends Controller
{
    public function index()
    {
        $result['lists'] = Busanatari::get();
        $result['cats'] = Category::get();
        $fx = $this;
        return view('admin.busanatari.lists', compact('result','fx'));
    }

    public function store(Request $request)
    {
        $fileName = time().'.'.$request->file->getClientOriginalExtension();  
        $request->file->move(public_path('uploads'), $fileName);
        Busanatari::create($request->except('file') + ['foto' => $fileName]);
        return redirect()->back()->with('status','success');
    }

    public function edit($id)
    {
        $result = Busanatari::find($id);

        return response()->json($result);
    }

    public function update(Request $request)
    {
        $result = Busanatari::find($request->id);
        if ($request->hasFile('file')) {
            $fileName = time().'.'.$request->file->getClientOriginalExtension();  
            $request->file->move(public_path('uploads'), $fileName);
            // Delete Old Image
            @unlink(public_path('uploads/'.$result->foto));
            $result->update($request->except(array('_token','file')) + ['foto' => $fileName]);
        }else{
            $result->update($request->except(array('_token','file')));
        }
        return redirect()->back()->with('status','success');
    }

    public function destroy($id)
    {
        $result = Busanatari::find($id)->delete();
        return redirect()->back()->with('status','success');
    }


    // Fitur Tambahan

    public function Rp($value='')
    {
        return 'Rp. '.number_format($value,0,'','.');
    }


    public function fotobusana($id_busana='')
    {
        $data['busana'] = Busanatari::find($id_busana);
        $data['foto'] = FotoBusana::where('id_busana',$id_busana)->get();

        return view('admin.busanatari.fotobusana', compact('data'));
    }

    public function fotobusana_store(Request $request)
    {
        if ($request->hasFile('file')) {
            // Delete OLD Data
            $olds = FotoBusana::where('id_busana',$request->id_busana)->get();
            foreach ($olds as $old) {
                @unlink(public_path('uploads/'.$old->foto));
                $old->delete();
            }
            $data = array();
            foreach ($request->file as $file) {
                $fileName = 'album-'.rand(11111,99999).'-'.time().'.'.$file->getClientOriginalExtension();  
                $file->move(public_path('uploads'), $fileName);
                $item = array('id_busana'=> $request->id_busana,'foto'=>$fileName);
                array_push($data, $item);
            }
            FotoBusana::insert($data);
        }
        return redirect()->back()->with('status','success');
    }

    public function fotobusana_destroy($id='')
    {
        $data = FotoBusana::find($id);
        @unlink(public_path('uploads/'.$data->foto));
        $data->delete();
        return redirect()->back()->with('status','success');
    }




}
