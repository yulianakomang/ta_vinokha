<?php

namespace App\Http\Middleware;

use Closure;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $roles = explode('-', $roles);

        if (!in_array($request->user()->level , $roles)) {
            // return response()->view('error');
            return abort(403);
        }

        return $next($request);
    }
}
