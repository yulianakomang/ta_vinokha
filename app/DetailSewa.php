<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailSewa extends Model
{
    protected $table ='tb_detailsewa';
    protected $guarded = [];

    public function getBusana()
    {
    	return $this->belongsTo('App\Busanatari','id_busana');
    }

}
