<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table ='tb_transaksi';
    protected $guarded = [];

    public function getSewa()
    {
    	return $this->belongsTo('App\Sewa','id_sewa');
    }
}
