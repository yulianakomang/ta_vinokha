<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['front.app','front.cart','front.checkout'], function(View $view) {
            $user = \Auth::user();
            $cart = \Session::get('cart');
            if ($cart==!null) {
                $cart_jml2 = count(\Session::get('cart'));
            } else {
                $cart_jml2 = '0';
            }

            // return
            $view->with('cart',$cart);
            $view->with('cart_jml',$cart_jml2);
            $view->with('user',$user);
        });
    }
}
